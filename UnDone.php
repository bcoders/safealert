<?php
/**
 * Created by PhpStorm.
 * User: Coder
 * Date: 2/26/2018
 * Time: 12:32 AM
 */

$pageName = "Undone Items";
require_once "inc/header.php";
RequireLoggedInAccess();
?>
<div class="row">
    <div class="col-md-11">
        <a href="NewCrime.php" class="btn btn-primary">Add New ToDo</a>
    </div>
</div>
<hr/>
<div class="row">
    <div class="col-md-4">
        <div class="panel panel-warning">
            <div class="panel-heading">
                <b>My undone items</b>
            </div>
            <div class="panel-body">
                (Here is a list of items that I've not yet done)
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="panel panel-success">
            <div class="panel-heading">
                <b>My completed items</b>
            </div>
            <div class="panel-body">
                (Here is a list of the items I've completed already)
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="panel panel-info">
            <div class="panel-heading">
                <b>My postponed items</b>
            </div>
            <div class="panel-body">
                (Here is a list of the items I've postponed to a later date)
            </div>
        </div>
    </div>
</div>

<?php
require_once "inc/footer.php";