<?php
/**
 * Created by PhpStorm.
 * User: Coder
 * Date: 10/22/2018
 * Time: 1:38 PM
 */

/**
 * declare the page name that is passed to our header file, which is the included in this file
 *
 */
$pageName = 'Crime';
require_once 'inc/header.php';

// our search sidebar
require_once 'inc/LeftSidebar.inc';

/**
 * we save server resources by using a $_GET parameter to be parsed
 * problem is they may be abused by users (with some web knowledge) to view sensitive info
 * we therefore need to properly validate this file
 * each parameter (including NULL) is validated
 */
/**
 * we begin by checking if a $_GET parameter exists and collect all the values therein
 * we save them into variables
 * I know all the variables I set (hard coded into the program)
 * I check them here for existence
 */
if (!isset($_GET))
{$functions->redirect('index.php');} # this page can only be accessed if there is a $_REQUEST parameter

/**
 * instead of re-checking the $_GET variable twice,
 * I just start querying for the values therein
 */



// let's handle a single crime first (when clicking the 'View Crime' link)
if (isset($_GET['CiD']) && $_GET['CiD'] != NULL){
    $crimeID  =  base64_decode($_GET['CiD']);
    $stmt     =  $db->row("SELECT * FROM `crimes` WHERE `ID` = :ID AND NOT (ID = '')", array('ID'=>$crimeID));

    // there's actually a crime
    if ($stmt > 0):
    ?>
        <div class="row single-crime" id="single-crime">
    <div class="col-md-10 crime-contents">
        <div class="panel panel-info">
            <div class="panel-heading">
                A <?php echo '<b class="slug">' . ucfirst($stmt['slug']) . '</b>' . ' crime committed on <b class="time">' . $stmt['dateOccurred'] . '</b><br/>';
                echo 'The crime occurred at <b class="location">' . $stmt['location'] . '</b>';
                ?>
            </div>
            <div class="panel-body">
                <?php echo ucfirst($stmt['description']);?>
            </div>
        </div>
    </div>

    <div class="col-md-1 r-articles">
        <div class="navbar-header related-articles">
            RELATED ARTICLES
        </div>
            <p><div class=""><br/><br/></div>
        <?php
        /**
         * we get 5 random posts relates to the current crime
         * we use the crime location and slug to make the relation
         */
        $crimeID  =  base64_decode($_GET['CiD']);
        $slug     = $stmt['slug'];
        $location = $stmt['location'];
        $relatedQ = $db->query("SELECT `ID`, `description`, `slug`, `timeOccurred` FROM `crimes` WHERE `slug` LIKE :slug OR `location` LIKE :location AND `ID` != :crimeID LIMIT  6",array('slug'=>"%$slug%", 'location'=>"%$location%", 'crimeID'=>$crimeID));
        // print them like links pointing to the crimes themselves, back to this page
        foreach ($relatedQ as $item):
            # truncate the description further than that of the index page
            $fullDescription = $item['description'];
            if (strlen($fullDescription) > 70):

                $pos = strpos($fullDescription, ' ', 60);
                ?>
                <h4 class="" style="font-size: 17px;
line-height: 24px;margin-bottom: 7px;font-weight: 500;">
                <a class="r-href" href="Crime.php?CiD=<?php echo base64_encode($item['ID']); ?>&description=<?php echo base64_encode($item['slug']); ?>&occt=<?php echo base64_encode($item['timeOccurred']);?>"><?php echo substr($fullDescription, 0, $pos).'...' ;?></a></h4>
                <?php else:?>
                <h4 class="" style="font-size: 17px;
line-height: 24px;margin-bottom: 7px;font-weight: 500;">
                    <a class="r-href" href="Crime.php?CiD=<?php echo base64_encode($item['ID']); ?>&description=<?php echo base64_encode($item['slug']); ?>&occt=<?php echo base64_encode($item['timeOccurred']);?>"><?php echo ($fullDescription).'...' ;?></a> </h4>
            <?php
                endif;
    // end foreach here
    endforeach;
    ?></p>
        </div>
        </div>
</div>

        <?php
          endif;

    // end if for $_GET['CiD']
}
/** ---------------- **/

/**
 * begin the search function on the sidebar
 */

/**
 *
 * if user searched only by location
 * means other $_GET params are NULL
 *
 * */
if (isset($_GET['sr-location']) && $_GET['sr-location'] != NULL && $_GET['sr-type'] == NULL && $_GET['sr-date'] == NULL )
{
    $location = $_GET['sr-location'];
    $srLocation = $db->query("SELECT * FROM `crimes` WHERE `location` LIKE :location AND NOT (slug IS NULL OR dateOccurred IS NULL OR crimes.dateOccurred IS NULL)", array('location'=>"%$location%"));
    if ($srLocation && $srLocation > 0){
    	?>
    	
        <div class="crime-details sr-location">
        <div class="text-left sr-heading" style="font-size: 120%;padding: 6px;">
            Showing results for <b>Location:</b><span class="slug"><?php echo $_GET['sr-location'];?></span>
        </div>
        <?php
        include_once "inc/SearchLocation.php";
    }
    else{
        ?>
        <div class="row">
        <div class=col-md-2></div>
        <div class="col-md-6">
            <div class="panel panel-warning">
                <div class="panel-body">
                    We could not find anything for the query provided. Please try again
                </div>
            </div>
        </div>
    </div>
        <?php
    }

}
// end sr-location 'if'


/**
 * lets now deal with only the crime type
 */
if (isset($_GET['sr-type']) && $_GET['sr-type'] != NULL && $_GET['sr-location'] == NULL && $_GET['sr-date'] == NULL)
{
    ?>
    
    <?php
    $cType  = $_GET['sr-type'];
    $srType = $db->query("SELECT * FROM `crimes` WHERE `slug` LIKE :cType AND NOT (slug = '')", array('cType'=>"%$cType%"));
    if ($srType)
    {
    	?>
    	<div class="row crime-details">
        <div class="col-md-9 sr-type">
    <div class="text-left sr-heading" style="font-size: 120%;padding: 6px;margin-left: 10px;">
        Showing results for <span class="slug">"<?php echo $_GET['sr-type'];?>" </span> crimes:
    </div>
    <?php
        include_once "inc/SearchByCrimeType.php";
    }
    else{
        ?>
        </div>
        </div>
        <div class="row">
        <div class="col-md-2"></div>
            <div class="col-md-9">
                <div class="panel panel-warning">
                    <div class="panel-body">
                        We could not find anything for the query provided. Please try again
                    </div>
                </div>
            </div>
        </div>
        <?php
    }

}
// end sr-type 'if'

/**
 * lets search for only the date
 */
if (isset($_GET['sr-date']) && $_GET['sr-date'] != NULL && $_GET['sr-location'] == NULL && $_GET['sr-type'] == NULL)
{
    $cDate  = $_GET['sr-date'];
    $srDate = $db->query("SELECT * FROM `crimes` WHERE `dateOccurred` = :cDate AND NOT ( dateOccurred = '') ", array('cDate'=>$cDate));
    if ($srDate)
    {
    	?>
    	<div class="row sr-date" style="margin-left: 250px; margin-right: 0">
    <div class="text-left sr-heading" style="font-size: 120%;padding: 6px;">
        Showing results for date <span class="slug"><?php echo $_GET['sr-date'];?></span>
    </div>
    <?php
        include_once "inc/SearchByCrimeDate.php";
    }
    else{
        ?>
        <div class="row">
            <div class="col-md-9">
                <div class="panel panel-warning">
                    <div class="panel-body">
                        We could not find anything for the query provided. Please try again
                    </div>
                </div>
            </div>
        </div>
        <?php
    }

}
// end sr-date 'if'

?>
        <div class="row crime-details" style="">
            <?php
/**
 * try searching where all the search params are involved
 * and all the $_GET params are set
 */
if (isset($_GET['sr-location']) && isset($_GET['sr-date']) && isset($_GET['sr-type']) && $_GET['sr-location'] != '' && $_GET['sr-date'] != '' && $_GET['sr-type'] != '')
{
    $AllLocation = $_GET['sr-location'];
    $AllDate     = $_GET['sr-date'];
    $AllType     = $_GET['sr-type'];
    $sth  =  $db->query("SELECT * FROM `crimes` WHERE `slug` LIKE :slug AND `dateOccurred` LIKE :dateOccurred AND `location` LIKE :location AND (slug IS NULL OR dateOccurred IS  NULL OR dateOccurred IS NULL OR location IS NULL )", array('slug'=>"%$AllType%", 'dateOccurred'=>"%$AllDate%", 'location'=>"%$AllLocation%") );
    if ($sth)
    {
        // there is something that is returned
        /** @var TYPE_NAME $single */
        foreach ($sth as $single) :
            ?>
            <div class="col-md-9 sr-location">

                <div class="panel panel-info">
                    <div class="panel-heading">
                        A <?php echo '<b class="slug">' . ucfirst($single['slug']) . '</b>' . ' crime committed on <b class="time">' . $single['dateOccurred'] . '</b><br/>';
                        echo 'The crime occurred at <b class="location">' . $single['location'] . '</b>';
                        ?>
                    </div>
                    <div class="panel-body">
                        <?php
                        $fullDescription = $single['description'];
                        if (strlen($fullDescription) > 699)
                        {
                            $pos = strpos($fullDescription, ' ', 700);
                            echo '<p>' . substr($fullDescription, 0, $pos) . '......</p>';?>
                            <a href="Crime.php?CiD=<?php echo base64_encode($single['ID']); ?>&description=<?php echo base64_encode($single['slug']); ?>&occt=<?php echo base64_encode($single['timeOccurred']);?>" class='btn btn-info'> View Crime</a>
                            <?php
                        }
                        else
                        {
                            echo '<p>' . $fullDescription . '</p>';
                        }
                        ?>
                    </div>
                </div>
            </div>
        <?php
            // endo sPlace foreach
        endforeach;
    }
    else{
    ?>
    </div>
    <div class="col-md-12">
        <div class="panel panel-warning">
            <div class="panel-body">
                We could not find anything for the query provided. Please try again
            </div>
        </div>
    </div>
<?php
    }

}
elseif (isset($_REQUEST['sr-location']) && $_REQUEST['sr-location'] === NULL){
    ?>
    </div>
    <div class="col-md-12">
        <div class="panel panel-warning">
            <div class="panel-body">
                We could not find anyting for the query provided. Please try again
            </div>
        </div>
    </div>
    <?php
}

/**
 * we get the mobile search query
 * this is displayed on top of a device whose min-width is less then 998px
 */
if (isset($_GET['q']) && $_GET['q'] != ''){
    include_once "inc/MobileSearch.php";
}
elseif (isset($_GET['q']) && $_GET['q'] == '')
{
    echo <<<EOT
    <div class="col-md-12">
        <div class="panel panel-warning">
            <div class="panel-body">
We could not find anything for the query provided. Please try again
</div>
        </div>
    </div>
EOT;
}
?>

<?php
include_once "inc/footer.php";
?>