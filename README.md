# safeAlert

An online based crime reporting system where users can report crimes that occur around them. 
It all aggregates to a common 'pool' of crimes where a single location or time or crime can be viewed. It will eventually help identify hot-spots for crimes or even identify when most crimes occur.
I hope a mobile app comes later for this app.

# How To Install (version 0.1)
1. Clone the repository you want to use. I got the password since it's a private repo. There are two repositories:
    a) The "master" - which is always stable and updated after a random times upon a new 'feature' from the 'dev' branch
    b) The 'dev' branch - This is usually like the 'nightly' version. It's always under construction and when a change is final, it's pushed to the master branch.
2. After cloning, open phpMyadmin and import the "krime.sql" file which will handle your database creation and also insert some data to begin with.
3. Navigate to "<your server>/krime" to access the project
4. Create a user account, log in and begin recording crimes
                                 /*******/
