<?php
/**
 * Created by PhpStorm.
 * User: Coder
 * Date: 11/18/2018
 * Time: 5:29 PM
 */

class SafeAlert Extends Db
{
    # @object, the page name
    private $page;


    /**
     * @param mixed $page
     */
    public function setPage($page)
    {
        $this->page = $page;
    }

    /**
     * @return mixed
     */
    public function getPage()
    {
        return $this->page;
    }


    public function getPageContent($pageName)
    {
        /**
         * for now since we did not sanitize() the contents when we were saving it to db,
         * we just get the contents 'plainly'
         * they should be sanitized() though, and decoded back here
         * I'm working on that though
         */
        # fetch everything from the db
        $pgCntArray = parent::query("SELECT * FROM `pages` WHERE `slug` = :slug", array('slug'=>$pageName));
        foreach ($pgCntArray as $content){
            $pageContent = $content['content'];
        }
        echo "<div class=\"page-content single-page\">
    $pageContent
</div>
";

    }
}

$weAreSafer = new SafeAlert();