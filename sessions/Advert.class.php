<?php
/**
 * Created by PhpStorm.
 * User: Coder
 * Date: 11/10/2018
 * Time: 6:18 AM
 * @param int $limit
 */
include_once "Db.class.php";

class Advert Extends Db{

    public $functions;

    /**
     * @param int $limit
     * @return mixed
     */
    public function GetAdverts($limit = 9){
        include_once ('Db.class.php');
        $settings = parse_ini_file('settings.ini.php');
        /**
         * we get only the active adverts
         * we also get if there is any image file associated with the ad
         * we only get the ads which have not passed the validity period
         * we then place them on the right panel
         */

            $today         = date('Y-m-d');
            $getQuery      =  Db::query("SELECT * FROM adverts WHERE `active` = 1 AND `validTill` >= :today LIMIT $limit", array('today'=>"$today"));
            foreach ($getQuery as $singleAd):
                $adName    = $singleAd['name'];
                $adContent = $singleAd['content'];
                $adImage   = $singleAd['image'];
                $adURL     = $singleAd['link'];
            ?>
            <a href="<?php echo $adURL;?>" target="_blank" class="thumbnail ad">
                <div class="caption ad-header"><?php echo $adName;?></div>
                <div class="thumbnail">
                    <img src="<?php echo $settings['advertImg'] . $adImage?>"/>
                </div>
                <div class="add-caption"><?php echo $adContent; ?></div>
            </a>

            <?php
            endforeach;

    }

    // end class
}
// create a new instance of the class
$advert = new Advert();