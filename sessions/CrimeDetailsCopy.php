<?php
/**
 * Created by PhpStorm.
 * User: Coder
 * Date: 10/29/2018
 * Time: 1:28 PM
 */

/**
 * we find out
 */
$crimesDumpArray = $db->query(" SELECT * FROM `crimes` ORDER BY `ID` DESC ");

/**
 * for each crime, we get the specific details of the crime (using our pre-coded functions.php
 */
foreach ($crimesDumpArray as $item):
    // get the userID and get the username of the user who posted the crime
    $userName = $functions->GetUserNameByID($item['postedByID']);
    $postingdate  = $functions->GetCrimePostingDate($item['ID']);
    ?>
    <div class="panel panel-info" id="crime-list">
        <div class="panel-heading text-right" style="font-style: italic;">
            <i class="fa fa-bullhorn"></i>&nbsp;Posted by <?php echo '<b>' . ucwords($userName) . '</b> on <b>' . $postingdate . '</b>';?>
        </div>
        <div class="panel-heading" style="color:#31708f;background-color: #e8e2f7;border-radius: 0;font-size: x-large;">
            <?php echo $item['slug'];?>
        </div>
        <div class="panel-body">
            <?php
            $fullDescription = $item['description'];
            if (strlen($fullDescription) > 699)
            {
                $pos = strpos($fullDescription, ' ', 700);
                echo '<p>' . substr($fullDescription, 0, $pos) . '......</p>';?>
                <a href="Crime.php?CiD=<?php echo base64_encode($item['ID']); ?>&description=<?php echo base64_encode($item['slug']); ?>&occt=<?php echo base64_encode($item['timeOccurred']);?>" class='btn btn-info'> View Crime</a>
                <?php
            }
            else {
                echo $fullDescription;
            }
            ?>
        </div>
    </div>

<?php
endforeach;
/**
 * we shouldn't display all the results in one page
 * let's try to do some pagination here
 * we'll limit our results to 8 crimes
 * the rest will be shown as more pages
 */
