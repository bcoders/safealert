<?php
/**
 * Created by PhpStorm.
 * User: Coder
 * Date: 2/24/2018
 * Time: 7:13 AM
 */

require_once "Db.class.php";

class Functions extends Db
{
    public $functions;


    /**
     * @param $data
     * @return string
     *     @description
     *     sanitizes the $data provided
     *
     */
    public function sanitise($data)
    {
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        $data = trim($data);
        //$data = stripslashes($data);  // i think this one will affect emails

        return $data;
    }

    /**
     * @void
     * @param $url
     * redirects to the target $url
     */
    public function redirect($url){
        header("Location: $url");
    }

    /**
     * @void
     * Created for pages that require logged In access only
     * if a user is not logged in they are redirected to the previous page
     * or the home page if a referrer address does not exist
     */
    public function RequireLoggedInAccess()
    {
        // if this functions appears in a page, it can only be accessed by a logged in user

        if (!isset($_SESSION['loggedInToUs']))
        {
            //first, redirect to a previous page if it exists
            if (isset($_SERVER['HTTP_REFERER']))
            {
                $this->redirect('SignIn.php');
            }
            else{
                $this->redirect('SignIn.php');
            }
        }

        return False;
    }

    /**
     * @void
     * Created for pages that require exlusively logged Out access only
     * if a user is not logged in they are redirected to the previous page
     * or the home page if a referrer address does not exist
     */
    public function RequireLoggedOutAccess()
    {
        // if this functions appears in a page, it can only be accessed by a logged in user

        if (isset($_SESSION['loggedInToUs']))
        {
            //first, redirect to a previous page if it exists
            if (isset($_SERVER['HTTP_REFERER']))
            {
                $this->redirect($_SERVER['HTTP_REFERER']);
            }
            else{
                $this->redirect('index.php');
            }
            return True;
        }
    }

    /**
     * @param $id
     * @return mixed
     */
    public function GetUserNameByID($id)
    {

        return  parent::single("SELECT `username` FROM `users` WHERE `ID` = :id", array('id'=>$id));
    }

    /**
     * @param $id
     * @return string
     */
    public function GetCrimePostingDate($id){
        return parent::single("SELECT `datePosted` FROM `crimes` WHERE `ID` = :id", array('id'=>$id));
    }

    /**
     * @param $userEmail
     * @return string
     */
    public function getUserIDFromEmail($userEmail){
        return parent::single("SELECT `ID` FROM `users` WHERE `email` = :userEmail", array('userEmail'=>$userEmail));
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function SetGlobalCookie(){
        if (isset($globalCookie))
        {
            return $globalCookie;
        }
        else{
            return setrawcookie('_GlCk', md5(random_bytes(1200)));
        }
    }

    public function encodeData($data)
    {
        $input  = $data;

        $data .= str_pad($input, 2, '0', STR_PAD_BOTH);

        $data .= substr(strtr(($input), '+', '.'), 0, 220);

        return $data;
    }

    public function decodeData($salt='blancos')
    {

    }

    public function SearchBy($table){

    }

    public function GetBrowser(){
        if (isset($_SERVER['HTTP_USER_AGENT'])){
            $userAgent = $_SERVER['HTTP_USER_AGENT'];
        }
        else{
            $userAgent = NULL;
        }
        return $userAgent;
    }

    public function GetReferringURL(){
        if (isset($_SERVER['HTTP_REFERER'])){
            $referrer = $_SERVER['HTTP_REFERER'];
        }
        else{
            $referrer = NULL;
        }
        return $referrer;
    }

}

$functions = new Functions();