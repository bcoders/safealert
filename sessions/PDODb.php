<?php
/**
 * Created by PhpStorm.
 * User: Coder
 * Date: 2/21/2018
 * Time: 5:03 AM
 */
/**
 * start a session here
 * it is necessary since this file will be used on all the website
 * for redundancy issues and security, we call the session here
 * and include this file at the top of 'header.php'
 */
//session_start();

$dbHost     = 'localhost';
$dbName     = 'ToDo';
$dbUser     = 'blancos';
$dbPass     = 'blancoskhim';
$dbCharset  = 'utf8mb4';


$dsn = "mysql:host=$dbHost;dbname=$dbName;charset=$dbCharset";
$options = array(
    PDO::ATTR_ERRMODE=>PDO::ERRMODE_EXCEPTION,
    PDO::ATTR_DEFAULT_FETCH_MODE=>PDO::FETCH_ASSOC,
    PDO::ATTR_EMULATE_PREPARES=>false
);

$PDO = new PDO($dsn, $dbUser, $dbPass, $options);