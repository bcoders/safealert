<?php
/**
 *  DB - A simple database class 
 *
 * @author		Author: Vivek Wicky Aswal. (https://twitter.com/#!/VivekWickyAswal)
 * @git 		https://github.com/wickyaswal/PHP-MySQL-PDO-Database-Class
 * @version      0.2ab
 *
 */

class DBExtended extends Db
{
	# @object, the db host
	private $mysqlHost = 'localhost';
	
	# @object, the db name
	private $dbName    = 'krime';
	
	# @object, the db user
	private $dbUser    = 'blancos';
	
	# @object, the db password
	private $dbPass    = 'blancoskhim';

	/**
	* This method will return the dbh as a string
	* 
	* @return string
	*/    
    public function DbhParams(){
		$dbh = "mysql:host=$mysqlHost;dbname=$dbName" . ',' . "$dbUser" . ',' . "$dbPass";
		
		return($dbh);
	}
   
}

$dbE = new DBExtended();
?>
