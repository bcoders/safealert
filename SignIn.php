<?php
/**
 * Created by PhpStorm.
 * User: Coder
 * Date: 2/23/2018
 * Time: 5:27 AM
 */
$pageName = "Log In";
require_once "inc/header.php";
require_once 'inc/LeftSidebar.inc';
/**
 * allow only logged out users to access this page
 */
$functions->RequireLoggedOutAccess();

// validating the user input
if (isset($_POST['login']))
{
    // this file needs the database classes to insert read and do db operations
    require_once 'sessions/Db.class.php';
    // we also need the bcrypt class to hash our password
    require_once "sessions/Bcrypt.php";
    // this file handles our sanitization issues
    require_once 'sessions/functions.php';

    $email     = $_POST['email'];
    $password  = $_POST['password'];
    $response  = $_POST["g-recaptcha-response"];
    
    $url = 'https://www.google.com/recaptcha/api/siteverify';
	$data = array(
		'secret' => '6Ldk0n8UAAAAAJOley79M8ooRTS2hMEl5VfcU5sL',
		'response' => $_POST["g-recaptcha-response"]
	);
	$options = array(
		'http' => array (
			'method' => 'POST',
			'content' => http_build_query($data)
		)
	);
	$context  = stream_context_create($options);
	$verify = file_get_contents($url, false, $context);
	$captcha_success=json_decode($verify);
	
	
    if (empty($_POST['email']))
    {
        $oError[] = "You must provide an email address";
    }
    elseif (!filter_var($email,FILTER_VALIDATE_EMAIL))
    {
        $oError[] = "Invalid email format";
    }
    elseif (empty($_POST['password']))
    {
        $oError[] = "You must provide a password";
    }
    elseif($captcha_success->success==false){
		$oError[] = "We couldn't verify you are human";
	}
    // else everything above is correct
    else{
        // check for valid email / password combination
        // in case for any error, we don't reveal anything to the user, just a custom message for all errors

        // first do the creds exist?
        if (!$db->single("SELECT email FROM users WHERE email= :email", array("email"=>$email)))
        {
            $realErrorMsg[] = "Email does not exist";
            $oError[] = "Sorry! Wrong username and password combination";
        }
        else{
            // the user exists, so we continue
            // get the user's hash from db
            $passHashfromdb = $db->single("SELECT password FROM users WHERE email= :email",array("email"=>$email));
            // we check the hash against the supplied password
            if (!Bcrypt::checkPassword($password, $passHashfromdb))
            {
                $realErrorMsg[] = "The password you provided is incorrect";
                $oError[] = "Sorry! Wrong username and password combination";
            }
            else{
                // we can now log in the user and create a session for them
                // login/session cookies are also created here
                $_SESSION['loggedInToUs']  = True;
                $_SESSION['userEmail'] = $email;
                $functions->redirect("index.php");
            }
        }
    }
}
?>
<div class="row">
    <div class="col-md-12 login-form">
        <div class="form-box">
            <div class="form-top" style="margin-top:10px;">
                <div class="form-top-left">
                    <span class="form-header">LOGIN</span>
                </div>
            </div>
            <?php
            if (isset($oError)):
                foreach ($oError as $error):?>
                    <div class="alert alert-danger col-md-12 col-sm-12 col-xs-12">&ensp;
                        <i class="fa fa-warning"></i>&nbsp;<?php echo $error . "!"; ?>
                    </div>
                <?php
                endforeach;
                endif;
                ?>
            <div class="form-bottom" style="padding: 50px">
                <form role="form" method="post" autocomplete="off">
                    <div class="form-group">
                        <label class="sr-only" for="form-email">Email</label>
                        <input type="email" name="email" placeholder="Email..." class="form-email form-control" id="form-email"
                        value="<?php if (isset($_POST['email'])){echo $email;} ?>" required="required" >
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="form-password">Password</label>
                        <input type="password" name="password" placeholder="Password..." class="form-password form-control" id="form-password" required="required">
                    </div>
                    <div class="form-group">
                    <label for="">Verify you are human</label>
                    	<div class="g-recaptcha" data-sitekey="6Ldk0n8UAAAAANiwctmahzfpVb5u8cmZ06qMPcvP"></div>
                    </div>
                    
                    <button type="submit" class="btn" name="login">Sign in!</button>
                </form>
            </div>
        </div>
    </div>
</div>

<?php
require_once "inc/footer.php";
