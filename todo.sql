-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 20, 2018 at 02:32 PM
-- Server version: 10.1.22-MariaDB
-- PHP Version: 7.1.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `todo`
--
CREATE DATABASE IF NOT EXISTS `todo` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `todo`;

-- --------------------------------------------------------

--
-- Table structure for table `adverts`
--

DROP TABLE IF EXISTS `adverts`;
CREATE TABLE `adverts` (
  `ID` int(10) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `content` text,
  `image` text,
  `link` text NOT NULL,
  `postingDate` text NOT NULL,
  `validTill` text NOT NULL,
  `active` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `adverts`
--

INSERT INTO `adverts` (`ID`, `name`, `content`, `image`, `link`, `postingDate`, `validTill`, `active`) VALUES
(1, 'Are You being spied upon? Help change the world', 'Join <b>The Free Internet Movement to campaign for a safer WEB', 'cctv-spying-on-us.jpg', 'http://blancoskhim.wordpress.com/category/stop-think-connect', '2018-11-10', '2018-11-24', 1),
(2, 'Download Quantum Firefox Browser For maximum online privacy', 'Firefox Browser is the Internet\'s most trusted security browser', 'tor-browser-advert.png', 'https://mozillaproject.org/downloads', '2018-11-10', '2018-11-15', 1),
(3, 'Are You being spied upon? Help change the world', 'Join <b>The Free Internet Movement to campaign for a safer WEB', 'FirefoxQuantum.png', 'http://blancoskhim.wordpress.com/category/stop-think-connect', '2018-11-10', '2018-11-13', 1),
(4, 'Get the best and secure operating system out there today', 'Windows 10 Ultimate is the most stable Operating System currently available today. Get your trial now', 'Windows-101-640x360.png', 'https://microsoft.com/?lang=en-us', '2018-11-10', '2018-11-12', 1);

-- --------------------------------------------------------

--
-- Table structure for table `crimes`
--

DROP TABLE IF EXISTS `crimes`;
CREATE TABLE `crimes` (
  `ID` int(10) NOT NULL,
  `postedByID` int(10) NOT NULL,
  `slug` text NOT NULL,
  `description` text NOT NULL,
  `datePosted` varchar(50) NOT NULL,
  `timeOccurred` varchar(10) NOT NULL,
  `dateOccurred` varchar(30) NOT NULL,
  `location` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `crimes`
--

INSERT INTO `crimes` (`ID`, `postedByID`, `slug`, `description`, `datePosted`, `timeOccurred`, `dateOccurred`, `location`) VALUES
(35, 3, '&lt;b&gt;Bold text&lt;/b&gt;', '&lt;i&gt;This is typed in italics&lt;/i&gt;', '2018 Nov Tue 14:45:27', '01:44', '2018-11-28', '&lt;'),
(36, 3, '&lt;b&gt;Bold text&lt;/b&gt;', '&lt;i&gt;This is typed in italics&lt;/i&gt;', '2018 Nov Tue 14:45:27', '01:44', '2018-11-28', '&lt;'),
(37, 3, '&lt;b&gt;Bold text&lt;/b&gt;', '&lt;i&gt;This is typed in italics&lt;/i&gt;', '2018 Nov Tue 14:45:27', '01:44', '2018-11-28', '&lt;'),
(39, 3, 'Testing aplabets only 22', 'This is to test if the ajax really works. I hope it does', '2018 Nov Tue 13:53:13', '00:52', '2018-11-28', 'Sample Place'),
(40, 3, '&lt;b&gt;Bold text&lt;/b&gt;', '&lt;i&gt;This is typed in italics&lt;/i&gt;', '2018 Nov Tue 14:45:27', '01:44', '2018-11-28', '&lt;'),
(41, 3, '&lt;b&gt;Bold text&lt;/b&gt;', '&lt;i&gt;This is typed in italics&lt;/i&gt;', '2018 Nov Tue 14:45:27', '01:44', '2018-11-28', '&lt;'),
(42, 3, '&lt;b&gt;Bold text&lt;/b&gt;', '&lt;i&gt;This is typed in italics&lt;/i&gt;', '2018 Nov Tue 14:45:27', '01:44', '2018-11-28', '&lt;'),
(43, 3, 'Testing aplabets only 22', 'This is to test if the ajax really works. I hope it does', '2018 Nov Tue 13:53:13', '00:52', '2018-11-28', 'Sample Place'),
(44, 3, '&lt;b&gt;Bold text&lt;/b&gt;', '&lt;i&gt;This is typed in italics&lt;/i&gt;', '2018 Nov Tue 14:45:27', '01:44', '2018-11-28', '&lt;'),
(45, 3, '&lt;b&gt;Bold text&lt;/b&gt;', '&lt;i&gt;This is typed in italics&lt;/i&gt;', '2018 Nov Tue 14:45:27', '01:44', '2018-11-28', '&lt;'),
(46, 3, '&lt;b&gt;Bold text&lt;/b&gt;', '&lt;i&gt;This is typed in italics&lt;/i&gt;', '2018 Nov Tue 14:45:27', '01:44', '2018-11-28', '&lt;'),
(47, 3, 'Testing aplabets only 22', 'This is to test if the ajax really works. I hope it does', '2018 Nov Tue 13:53:13', '00:52', '2018-11-28', 'Sample Place'),
(48, 3, '&lt;b&gt;Bold text&lt;/b&gt;', '&lt;i&gt;This is typed in italics&lt;/i&gt;', '2018 Nov Tue 14:45:27', '01:44', '2018-11-28', '&lt;'),
(49, 3, '&lt;b&gt;Bold text&lt;/b&gt;', '&lt;i&gt;This is typed in italics&lt;/i&gt;', '2018 Nov Tue 14:45:27', '01:44', '2018-11-28', '&lt;'),
(50, 3, '&lt;b&gt;Bold text&lt;/b&gt;', '&lt;i&gt;This is typed in italics&lt;/i&gt;', '2018 Nov Tue 14:45:27', '01:44', '2018-11-28', '&lt;'),
(51, 3, 'Testing aplabets only 22', 'This is to test if the ajax really works. I hope it does', '2018 Nov Tue 13:53:13', '00:52', '2018-11-28', 'Sample Place'),
(52, 3, '&lt;b&gt;Bold text&lt;/b&gt;', '&lt;i&gt;This is typed in italics&lt;/i&gt;', '2018 Nov Tue 14:45:27', '01:44', '2018-11-28', '&lt;'),
(53, 3, '&lt;b&gt;Bold text&lt;/b&gt;', '&lt;i&gt;This is typed in italics&lt;/i&gt;', '2018 Nov Tue 14:45:27', '01:44', '2018-11-28', '&lt;'),
(54, 3, '&lt;b&gt;Bold text&lt;/b&gt;', '&lt;i&gt;This is typed in italics&lt;/i&gt;', '2018 Nov Tue 14:45:27', '01:44', '2018-11-28', '&lt;'),
(55, 3, 'Testing aplabets only 22', 'This is to test if the ajax really works. I hope it does', '2018 Nov Tue 13:53:13', '00:52', '2018-11-28', 'Sample Place'),
(56, 3, '&lt;b&gt;Bold text&lt;/b&gt;', '&lt;i&gt;This is typed in italics&lt;/i&gt;', '2018 Nov Tue 14:45:27', '01:44', '2018-11-28', '&lt;'),
(57, 3, '&lt;b&gt;Bold text&lt;/b&gt;', '&lt;i&gt;This is typed in italics&lt;/i&gt;', '2018 Nov Tue 14:45:27', '01:44', '2018-11-28', '&lt;'),
(58, 3, '&lt;b&gt;Bold text&lt;/b&gt;', '&lt;i&gt;This is typed in italics&lt;/i&gt;', '2018 Nov Tue 14:45:27', '01:44', '2018-11-28', '&lt;'),
(59, 3, 'Testing aplabets only 22', 'This is to test if the ajax really works. I hope it does', '2018 Nov Tue 13:53:13', '00:52', '2018-11-28', 'Sample Place'),
(60, 3, '&lt;b&gt;Bold text&lt;/b&gt;', '&lt;i&gt;This is typed in italics&lt;/i&gt;', '2018 Nov Tue 14:45:27', '01:44', '2018-11-28', '&lt;'),
(61, 3, '&lt;b&gt;Bold text&lt;/b&gt;', '&lt;i&gt;This is typed in italics&lt;/i&gt;', '2018 Nov Tue 14:45:27', '01:44', '2018-11-28', '&lt;'),
(62, 3, '&lt;b&gt;Bold text&lt;/b&gt;', '&lt;i&gt;This is typed in italics&lt;/i&gt;', '2018 Nov Tue 14:45:27', '01:44', '2018-11-28', '&lt;'),
(63, 3, 'Testing aplabets only 22', 'This is to test if the ajax really works. I hope it does', '2018 Nov Tue 13:53:13', '00:52', '2018-11-28', 'Sample Place'),
(64, 3, '&lt;b&gt;Bold text&lt;/b&gt;', '&lt;i&gt;This is typed in italics&lt;/i&gt;', '2018 Nov Tue 14:45:27', '01:44', '2018-11-28', '&lt;'),
(65, 3, '&lt;b&gt;Bold text&lt;/b&gt;', '&lt;i&gt;This is typed in italics&lt;/i&gt;', '2018 Nov Tue 14:45:27', '01:44', '2018-11-28', '&lt;'),
(66, 3, '&lt;b&gt;Bold text&lt;/b&gt;', '&lt;i&gt;This is typed in italics&lt;/i&gt;', '2018 Nov Tue 14:45:27', '01:44', '2018-11-28', '&lt;'),
(67, 3, 'Testing aplabets only 22', 'This is to test if the ajax really works. I hope it does', '2018 Nov Tue 13:53:13', '00:52', '2018-11-28', 'Sample Place'),
(68, 3, '&lt;b&gt;Bold text&lt;/b&gt;', '&lt;i&gt;This is typed in italics&lt;/i&gt;', '2018 Nov Tue 14:45:27', '01:44', '2018-11-28', '&lt;'),
(69, 3, '&lt;b&gt;Bold text&lt;/b&gt;', '&lt;i&gt;This is typed in italics&lt;/i&gt;', '2018 Nov Tue 14:45:27', '01:44', '2018-11-28', '&lt;'),
(70, 3, '&lt;b&gt;Bold text&lt;/b&gt;', '&lt;i&gt;This is typed in italics&lt;/i&gt;', '2018 Nov Tue 14:45:27', '01:44', '2018-11-28', '&lt;'),
(71, 3, 'Testing aplabets only 22', 'This is to test if the ajax really works. I hope it does', '2018 Nov Tue 13:53:13', '00:52', '2018-11-28', 'Sample Place'),
(72, 3, '&lt;b&gt;Bold text&lt;/b&gt;', '&lt;i&gt;This is typed in italics&lt;/i&gt;', '2018 Nov Tue 14:45:27', '01:44', '2018-11-28', '&lt;'),
(73, 3, '&lt;b&gt;Bold text&lt;/b&gt;', '&lt;i&gt;This is typed in italics&lt;/i&gt;', '2018 Nov Tue 14:45:27', '01:44', '2018-11-28', '&lt;'),
(74, 3, '&lt;b&gt;Bold text&lt;/b&gt;', '&lt;i&gt;This is typed in italics&lt;/i&gt;', '2018 Nov Tue 14:45:27', '01:44', '2018-11-28', '&lt;'),
(75, 3, 'Testing aplabets only 22', 'This is to test if the ajax really works. I hope it does', '2018 Nov Tue 13:53:13', '00:52', '2018-11-28', 'Sample Place'),
(76, 3, '&lt;b&gt;Bold text&lt;/b&gt;', '&lt;i&gt;This is typed in italics&lt;/i&gt;', '2018 Nov Tue 14:45:27', '01:44', '2018-11-28', '&lt;'),
(77, 3, '&lt;b&gt;Bold text&lt;/b&gt;', '&lt;i&gt;This is typed in italics&lt;/i&gt;', '2018 Nov Tue 14:45:27', '01:44', '2018-11-28', '&lt;'),
(78, 3, '&lt;b&gt;Bold text&lt;/b&gt;', '&lt;i&gt;This is typed in italics&lt;/i&gt;', '2018 Nov Tue 14:45:27', '01:44', '2018-11-28', '&lt;'),
(79, 3, 'Testing aplabets only 22', 'This is to test if the ajax really works. I hope it does', '2018 Nov Tue 13:53:13', '00:52', '2018-11-28', 'Sample Place'),
(80, 3, '&lt;b&gt;Bold text&lt;/b&gt;', '&lt;i&gt;This is typed in italics&lt;/i&gt;', '2018 Nov Tue 14:45:27', '01:44', '2018-11-28', '&lt;'),
(81, 3, '&lt;b&gt;Bold text&lt;/b&gt;', '&lt;i&gt;This is typed in italics&lt;/i&gt;', '2018 Nov Tue 14:45:27', '01:44', '2018-11-28', '&lt;'),
(82, 3, '&lt;b&gt;Bold text&lt;/b&gt;', '&lt;i&gt;This is typed in italics&lt;/i&gt;', '2018 Nov Tue 14:45:27', '01:44', '2018-11-28', '&lt;'),
(83, 3, 'Testing aplabets only 22', 'This is to test if the ajax really works. I hope it does', '2018 Nov Tue 13:53:13', '00:52', '2018-11-28', 'Sample Place'),
(84, 3, '&lt;b&gt;Bold text&lt;/b&gt;', '&lt;i&gt;This is typed in italics&lt;/i&gt;', '2018 Nov Tue 14:45:27', '01:44', '2018-11-28', '&lt;'),
(85, 3, '&lt;b&gt;Bold text&lt;/b&gt;', '&lt;i&gt;This is typed in italics&lt;/i&gt;', '2018 Nov Tue 14:45:27', '01:44', '2018-11-28', '&lt;'),
(86, 3, '&lt;b&gt;Bold text&lt;/b&gt;', '&lt;i&gt;This is typed in italics&lt;/i&gt;', '2018 Nov Tue 14:45:27', '01:44', '2018-11-28', '&lt;'),
(87, 3, 'Testing aplabets only 22', 'This is to test if the ajax really works. I hope it does', '2018 Nov Tue 13:53:13', '00:52', '2018-11-28', 'Sample Place'),
(88, 3, '&lt;b&gt;Bold text&lt;/b&gt;', '&lt;i&gt;This is typed in italics&lt;/i&gt;', '2018 Nov Tue 14:45:27', '01:44', '2018-11-28', '&lt;'),
(89, 3, '&lt;b&gt;Bold text&lt;/b&gt;', '&lt;i&gt;This is typed in italics&lt;/i&gt;', '2018 Nov Tue 14:45:27', '01:44', '2018-11-28', '&lt;'),
(90, 3, '&lt;b&gt;Bold text&lt;/b&gt;', '&lt;i&gt;This is typed in italics&lt;/i&gt;', '2018 Nov Tue 14:45:27', '01:44', '2018-11-28', '&lt;'),
(91, 3, 'Testing aplabets only 22', 'This is to test if the ajax really works. I hope it does', '2018 Nov Tue 13:53:13', '00:52', '2018-11-28', 'Sample Place'),
(92, 3, '&lt;b&gt;Bold text&lt;/b&gt;', '&lt;i&gt;This is typed in italics&lt;/i&gt;', '2018 Nov Tue 14:45:27', '01:44', '2018-11-28', '&lt;'),
(93, 3, '&lt;b&gt;Bold text&lt;/b&gt;', '&lt;i&gt;This is typed in italics&lt;/i&gt;', '2018 Nov Tue 14:45:27', '01:44', '2018-11-28', '&lt;'),
(94, 3, '&lt;b&gt;Bold text&lt;/b&gt;', '&lt;i&gt;This is typed in italics&lt;/i&gt;', '2018 Nov Tue 14:45:27', '01:44', '2018-11-28', '&lt;'),
(95, 3, '&lt;b&gt;Bold text&lt;/b&gt;', '&lt;i&gt;This is typed in italics&lt;/i&gt;', '2018 Nov Tue 14:45:27', '01:44', '2018-11-28', '&lt;'),
(96, 3, 'Testing aplabets only 22', 'This is to test if the ajax really works. I hope it does', '2018 Nov Tue 13:53:13', '00:52', '2018-11-28', 'Sample Place'),
(97, 3, '&lt;b&gt;Bold text&lt;/b&gt;', '&lt;i&gt;This is typed in italics&lt;/i&gt;', '2018 Nov Tue 14:45:27', '01:44', '2018-11-28', '&lt;'),
(98, 3, '&lt;b&gt;Bold text&lt;/b&gt;', '&lt;i&gt;This is typed in italics&lt;/i&gt;', '2018 Nov Tue 14:45:27', '01:44', '2018-11-28', '&lt;'),
(99, 3, '&lt;b&gt;Bold text&lt;/b&gt;', '&lt;i&gt;This is typed in italics&lt;/i&gt;', '2018 Nov Tue 14:45:27', '01:44', '2018-11-28', '&lt;'),
(100, 3, 'Testing aplabets only 22', 'This is to test if the ajax really works. I hope it does', '2018 Nov Tue 13:53:13', '00:52', '2018-11-28', 'Sample Place'),
(101, 3, '&lt;b&gt;Bold text&lt;/b&gt;', '&lt;i&gt;This is typed in italics&lt;/i&gt;', '2018 Nov Tue 14:45:27', '01:44', '2018-11-28', '&lt;'),
(102, 3, '&lt;b&gt;Bold text&lt;/b&gt;', '&lt;i&gt;This is typed in italics&lt;/i&gt;', '2018 Nov Tue 14:45:27', '01:44', '2018-11-28', '&lt;'),
(103, 3, '&lt;b&gt;Bold text&lt;/b&gt;', '&lt;i&gt;This is typed in italics&lt;/i&gt;', '2018 Nov Tue 14:45:27', '01:44', '2018-11-28', '&lt;'),
(104, 3, 'Testing aplabets only 22', 'This is to test if the ajax really works. I hope it does', '2018 Nov Tue 13:53:13', '00:52', '2018-11-28', 'Sample Place'),
(105, 3, '&lt;b&gt;Bold text&lt;/b&gt;', '&lt;i&gt;This is typed in italics&lt;/i&gt;', '2018 Nov Tue 14:45:27', '01:44', '2018-11-28', '&lt;'),
(106, 3, '&lt;b&gt;Bold text&lt;/b&gt;', '&lt;i&gt;This is typed in italics&lt;/i&gt;', '2018 Nov Tue 14:45:27', '01:44', '2018-11-28', '&lt;'),
(107, 3, '&lt;b&gt;Bold text&lt;/b&gt;', '&lt;i&gt;This is typed in italics&lt;/i&gt;', '2018 Nov Tue 14:45:27', '01:44', '2018-11-28', '&lt;'),
(108, 3, 'Testing aplabets only 22', 'This is to test if the ajax really works. I hope it does', '2018 Nov Tue 13:53:13', '00:52', '2018-11-28', 'Sample Place'),
(109, 3, '&lt;b&gt;Bold text&lt;/b&gt;', '&lt;i&gt;This is typed in italics&lt;/i&gt;', '2018 Nov Tue 14:45:27', '01:44', '2018-11-28', '&lt;'),
(110, 3, '&lt;b&gt;Bold text&lt;/b&gt;', '&lt;i&gt;This is typed in italics&lt;/i&gt;', '2018 Nov Tue 14:45:27', '01:44', '2018-11-28', '&lt;'),
(111, 3, '&lt;b&gt;Bold text&lt;/b&gt;', '&lt;i&gt;This is typed in italics&lt;/i&gt;', '2018 Nov Tue 14:45:27', '01:44', '2018-11-28', '&lt;'),
(112, 3, 'Testing aplabets only 22', 'I\'m writing this in my death. i can\'t even believe it is happening. I\'m Happy I can tell you how heaven is like. I hope you all find your way here. \r\nI\'m writing this in my death. i can\'t even believe it is happening. I\'m Happy I can tell you how heaven is like. I hope you all find your way here. \r\nI\'m writing this in my death. i can\'t even believe it is happening. I\'m Happy I can tell you how heaven is like. I hope you all find your way here. \r\nI\'m writing this in my death. i can\'t even believe it is happening. I\'m Happy I can tell you how heaven is like. I hope you all find your way here. \r\nI\'m writing this in my death. i can\'t even believe it is happening. I\'m Happy I can tell you how heaven is like. I hope you all find your way here. \r\nI\'m writing this in my death. i can\'t even believe it is happening. I\'m Happy I can tell you how heaven is like. I hope you all find your way here. \r\nI\'m writing this in my death. i can\'t even believe it is happening. I\'m Happy I can tell you how heaven is like. I hope you all find your way here. I\'m writing this in my death. i can\'t even believe it is happening. I\'m Happy I can tell you how heaven is like. I hope you all find your way here. I\'m writing this in my death. i can\'t even believe it is happening. I\'m Happy I can tell you how heaven is like. I hope you all find your way here. I\'m writing this in my death. i can\'t even believe it is happening. I\'m Happy I can tell you how heaven is like. I hope you all find your way here. \r\nI\'m writing this in my death. i can\'t even believe it is happening. I\'m Happy I can tell you how heaven is like. I hope you all find your way here. \r\nI\'m writing this in my death. i can\'t even believe it is happening. I\'m Happy I can tell you how heaven is like. I hope you all find your way here. \r\nI\'m writing this in my death. i can\'t even believe it is happening. I\'m Happy I can tell you how heaven is like. I hope you all find your way here. \r\nI\'m writing this in my death. i can\'t even believe it is happening. I\'m Happy I can tell you how heaven is like. I hope you all find your way here. \r\nI\'m writing this in my death. i can\'t even believe it is happening. I\'m Happy I can tell you how heaven is like. I hope you all find your way here. \r\nv', '2018 Nov Tue 13:53:13', '00:52', '2018-11-28', 'Sample Place'),
(113, 3, '&lt;b&gt;Bold text&lt;/b&gt;', '&lt;i&gt;This is typed in italics&lt;/i&gt;', '2018 Nov Tue 14:45:27', '01:44', '2018-11-28', '&lt;'),
(114, 3, '&lt;b&gt;Bold text&lt;/b&gt;', '&lt;i&gt;This is typed in italics&lt;/i&gt;', '2018 Nov Tue 14:45:27', '01:44', '2018-11-28', '&lt;'),
(115, 3, '&lt;b&gt;Bold text&lt;/b&gt;', '&lt;i&gt;This is typed in italics&lt;/i&gt;', '2018 Nov Tue 14:45:27', '01:44', '2018-11-28', '&lt;'),
(116, 3, 'Testing aplabets only 22', 'This is to test if the ajax really works. I hope it does', '2018 Nov Tue 13:53:13', '00:52', '2018-11-28', 'Sample Place'),
(117, 3, '&lt;b&gt;Bold text&lt;/b&gt;', '&lt;i&gt;This is typed in italics&lt;/i&gt;', '2018 Nov Tue 14:45:27', '01:44', '2018-11-28', '&lt;'),
(118, 3, '&lt;b&gt;Bold text&lt;/b&gt;', '&lt;i&gt;This is typed in italics&lt;/i&gt;', '2018 Nov Tue 14:45:27', '01:44', '2018-11-28', '&lt;'),
(119, 3, '&lt;b&gt;Bold text&lt;/b&gt;', '&lt;i&gt;This is typed in italics&lt;/i&gt;', '2018 Nov Tue 14:45:27', '01:44', '2018-11-28', '&lt;'),
(120, 3, '&lt;b&gt;Bold text&lt;/b&gt;', '&lt;i&gt;This is typed in italics&lt;/i&gt;', '2018 Nov Tue 14:45:27', '01:44', '2018-11-28', '&lt;'),
(121, 3, 'Testing aplabets only 22', 'This is to test if the ajax really works. I hope it does', '2018 Nov Tue 13:53:13', '00:52', '2018-11-28', 'Sample Place'),
(122, 3, '&lt;b&gt;Bold text&lt;/b&gt;', '&lt;i&gt;This is typed in italics&lt;/i&gt;', '2018 Nov Tue 14:45:27', '01:44', '2018-11-28', '&lt;'),
(123, 3, '&lt;b&gt;Bold text&lt;/b&gt;', '&lt;i&gt;This is typed in italics&lt;/i&gt;', '2018 Nov Tue 14:45:27', '01:44', '2018-11-28', '&lt;'),
(124, 3, '&lt;b&gt;Bold text&lt;/b&gt;', '&lt;i&gt;This is typed in italics&lt;/i&gt;', '2018 Nov Tue 14:45:27', '01:44', '2018-11-28', '&lt;'),
(125, 3, 'Testing aplabets only 22', 'This is to test if the ajax really works. I hope it does', '2018 Nov Tue 13:53:13', '00:52', '2018-11-28', 'Sample Place'),
(126, 3, '&lt;b&gt;Bold text&lt;/b&gt;', '&lt;i&gt;This is typed in italics&lt;/i&gt;', '2018 Nov Tue 14:45:27', '01:44', '2018-11-28', '&lt;'),
(127, 3, '&lt;b&gt;Bold text&lt;/b&gt;', '&lt;i&gt;This is typed in italics&lt;/i&gt;', '2018 Nov Tue 14:45:27', '01:44', '2018-11-28', '&lt;'),
(128, 3, '&lt;b&gt;Bold text&lt;/b&gt;', '&lt;i&gt;This is typed in italics&lt;/i&gt;', '2018 Nov Tue 14:45:27', '01:44', '2018-11-28', '&lt;'),
(129, 3, 'Testing aplabets only 22', 'This is to test if the ajax really works. I hope it does', '2018 Nov Tue 13:53:13', '00:52', '2018-11-28', 'Sample Place'),
(130, 3, '&lt;b&gt;Bold text&lt;/b&gt;', '&lt;i&gt;This is typed in italics&lt;/i&gt;', '2018 Nov Tue 14:45:27', '01:44', '2018-11-28', '&lt;'),
(131, 3, '&lt;b&gt;Bold text&lt;/b&gt;', '&lt;i&gt;This is typed in italics&lt;/i&gt;', '2018 Nov Tue 14:45:27', '01:44', '2018-11-28', '&lt;'),
(132, 3, '&lt;b&gt;Bold text&lt;/b&gt;', '&lt;i&gt;This is typed in italics&lt;/i&gt;', '2018 Nov Tue 14:45:27', '01:44', '2018-11-28', '&lt;'),
(133, 3, 'Testing aplabets only 22', 'This is to test if the ajax really works. I hope it does', '2018 Nov Tue 13:53:13', '00:52', '2018-11-28', 'Sample Place'),
(134, 3, '&lt;b&gt;Bold text&lt;/b&gt;', '&lt;i&gt;This is typed in italics&lt;/i&gt;', '2018 Nov Tue 14:45:27', '01:44', '2018-11-28', '&lt;'),
(135, 3, '&lt;b&gt;Bold text&lt;/b&gt;', '&lt;i&gt;This is typed in italics&lt;/i&gt;', '2018 Nov Tue 14:45:27', '01:44', '2018-11-28', '&lt;'),
(136, 3, '&lt;b&gt;Bold text&lt;/b&gt;', '&lt;i&gt;This is typed in italics&lt;/i&gt;', '2018 Nov Tue 14:45:27', '01:44', '2018-11-28', '&lt;'),
(137, 3, 'Testing aplabets only 22', 'This is to test if the ajax really works. I hope it does', '2018 Nov Tue 13:53:13', '00:52', '2018-11-28', 'Sample Place'),
(138, 3, '&lt;b&gt;Bold text&lt;/b&gt;', '&lt;i&gt;This is typed in italics&lt;/i&gt;', '2018 Nov Tue 14:45:27', '01:44', '2018-11-28', '&lt;'),
(139, 3, '&lt;b&gt;Bold text&lt;/b&gt;', '&lt;i&gt;This is typed in italics&lt;/i&gt;', '2018 Nov Tue 14:45:27', '01:44', '2018-11-28', '&lt;'),
(140, 3, '&lt;b&gt;Bold text&lt;/b&gt;', '&lt;i&gt;This is typed in italics&lt;/i&gt;', '2018 Nov Tue 14:45:27', '01:44', '2018-11-28', '&lt;'),
(141, 3, 'Testing aplabets only 22', 'This is to test if the ajax really works. I hope it does', '2018 Nov Tue 13:53:13', '00:52', '2018-11-28', 'Sample Place'),
(142, 3, '&lt;b&gt;Bold text&lt;/b&gt;', '&lt;i&gt;This is typed in italics&lt;/i&gt;', '2018 Nov Tue 14:45:27', '01:44', '2018-11-28', '&lt;'),
(143, 3, '&lt;b&gt;Bold text&lt;/b&gt;', '&lt;i&gt;This is typed in italics&lt;/i&gt;', '2018 Nov Tue 14:45:27', '01:44', '2018-11-28', '&lt;'),
(144, 3, '&lt;b&gt;Bold text&lt;/b&gt;', '&lt;i&gt;This is typed in italics&lt;/i&gt;', '2018 Nov Tue 14:45:27', '01:44', '2018-11-28', '&lt;'),
(145, 3, '&lt;b&gt;Bold text&lt;/b&gt;', '&lt;i&gt;This is typed in italics&lt;/i&gt;', '2018 Nov Tue 14:45:27', '01:44', '2018-11-28', '&lt;'),
(146, 3, 'Testing aplabets only 22', 'This is to test if the ajax really works. I hope it does', '2018 Nov Tue 13:53:13', '00:52', '2018-11-28', 'Sample Place'),
(147, 3, '&lt;b&gt;Bold text&lt;/b&gt;', '&lt;i&gt;This is typed in italics&lt;/i&gt;', '2018 Nov Tue 14:45:27', '01:44', '2018-11-28', '&lt;'),
(148, 3, '&lt;b&gt;Bold text&lt;/b&gt;', '&lt;i&gt;This is typed in italics&lt;/i&gt;', '2018 Nov Tue 14:45:27', '01:44', '2018-11-28', '&lt;'),
(149, 3, '&lt;b&gt;Bold text&lt;/b&gt;', '&lt;i&gt;This is typed in italics&lt;/i&gt;', '2018 Nov Tue 14:45:27', '01:44', '2018-11-28', '&lt;'),
(150, 3, 'Testing aplabets only 22', 'This is to test if the ajax really works. I hope it does', '2018 Nov Tue 13:53:13', '00:52', '2018-11-28', 'Sample Place'),
(151, 3, '&lt;b&gt;Bold text&lt;/b&gt;', '&lt;i&gt;This is typed in italics&lt;/i&gt;', '2018 Nov Tue 14:45:27', '01:44', '2018-11-28', '&lt;'),
(152, 3, '&lt;b&gt;Bold text&lt;/b&gt;', '&lt;i&gt;This is typed in italics&lt;/i&gt;', '2018 Nov Tue 14:45:27', '01:44', '2018-11-28', '&lt;'),
(153, 3, '&lt;b&gt;Bold text&lt;/b&gt;', '&lt;i&gt;This is typed in italics&lt;/i&gt;', '2018 Nov Tue 14:45:27', '01:44', '2018-11-28', '&lt;'),
(154, 3, 'Testing aplabets only 22', 'This is to test if the ajax really works. I hope it does', '2018 Nov Tue 13:53:13', '00:52', '2018-11-28', 'Sample Place'),
(155, 3, '&lt;b&gt;Bold text&lt;/b&gt;', '&lt;i&gt;This is typed in italics&lt;/i&gt;', '2018 Nov Tue 14:45:27', '01:44', '2018-11-28', '&lt;'),
(156, 3, '&lt;b&gt;Bold text&lt;/b&gt;', '&lt;i&gt;This is typed in italics&lt;/i&gt;', '2018 Nov Tue 14:45:27', '01:44', '2018-11-28', '&lt;'),
(157, 3, '&lt;b&gt;Bold text&lt;/b&gt;', '&lt;i&gt;This is typed in italics&lt;/i&gt;', '2018 Nov Tue 14:45:27', '01:44', '2018-11-28', '&lt;'),
(158, 3, 'Testing aplabets only 22', 'This is to test if the ajax really works. I hope it does', '2018 Nov Tue 13:53:13', '00:52', '2018-11-28', 'Sample Place'),
(159, 3, '&lt;b&gt;Bold text&lt;/b&gt;', '&lt;i&gt;This is typed in italics&lt;/i&gt;', '2018 Nov Tue 14:45:27', '01:44', '2018-11-28', '&lt;'),
(160, 3, '&lt;b&gt;Bold text&lt;/b&gt;', '&lt;i&gt;This is typed in italics&lt;/i&gt;', '2018 Nov Tue 14:45:27', '01:44', '2018-11-28', '&lt;'),
(161, 3, '&lt;b&gt;Bold text&lt;/b&gt;', '&lt;i&gt;This is typed in italics&lt;/i&gt;', '2018 Nov Tue 14:45:27', '01:44', '2018-11-28', '&lt;'),
(162, 3, 'Testing aplabets only 22', 'This is to test if the ajax really works. I hope it does', '2018 Nov Tue 13:53:13', '00:52', '2018-11-28', 'Sample Place'),
(163, 3, '&lt;b&gt;Bold text&lt;/b&gt;', '&lt;i&gt;This is typed in italics&lt;/i&gt;', '2018 Nov Tue 14:45:27', '01:44', '2018-11-28', '&lt;'),
(164, 3, '&lt;b&gt;Bold text&lt;/b&gt;', '&lt;i&gt;This is typed in italics&lt;/i&gt;', '2018 Nov Tue 14:45:27', '01:44', '2018-11-28', '&lt;'),
(165, 3, '&lt;b&gt;Bold text&lt;/b&gt;', '&lt;i&gt;This is typed in italics&lt;/i&gt;', '2018 Nov Tue 14:45:27', '01:44', '2018-11-28', '&lt;'),
(166, 3, 'Testing aplabets only 22', 'This is to test if the ajax really works. I hope it does', '2018 Nov Tue 13:53:13', '00:52', '2018-11-28', 'Sample Place'),
(167, 3, '&lt;b&gt;Bold text&lt;/b&gt;', '&lt;i&gt;This is typed in italics&lt;/i&gt;', '2018 Nov Tue 14:45:27', '01:44', '2018-11-28', '&lt;'),
(168, 3, 'I was killed', 'I\'m writing this in my death. i can\'t even believe it is happening. I\'m Happy I can tell you how heaven is like. I hope you all find your way here.', '2018 Nov Tue 14:03:09', '00:14', '2008-01-06', 'Behing Jericho, right after the intersection at Bethel'),
(169, 3, 'Posted from Android phone', 'Test to see if the ajax will Auto refresh on posting it to appear on the top list.. i hope it works', '2018 Nov Tue 18:29:45', '21:30', '2018-11-14', 'Kibingoti'),
(170, 3, 'Another test', 'The previous test auto refreshed so the Ajax is working fine.. I think I\'m gonna remove auto complete on this form. It\'s looking akward to me but as usual the end user comes first.\r\n\r\nSo I\'m gonna first go through UX fundamentals about forms and see which is the best practice for autocompletion...', '2018 Nov Tue 18:33:57', '21:33', '2018-11-13', 'Still at Kibingoti. Actually on my bed');

-- --------------------------------------------------------

--
-- Table structure for table `locations`
--

DROP TABLE IF EXISTS `locations`;
CREATE TABLE `locations` (
  `ID` int(10) NOT NULL,
  `name` text NOT NULL,
  `slug` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `locations`
--

INSERT INTO `locations` (`ID`, `name`, `slug`) VALUES
(1, 'kasarani', 'kasarani'),
(2, 'kawangware', 'kawangware');

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

DROP TABLE IF EXISTS `pages`;
CREATE TABLE `pages` (
  `ID` int(13) NOT NULL,
  `name` varchar(100) NOT NULL,
  `slug` varchar(20) NOT NULL,
  `content` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='The project pages';

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`ID`, `name`, `slug`, `content`) VALUES
(1, 'about us', 'about-us', 'Etiam nec odio vestibulum est mattis effic iturut magna. Pellentesque sit am et tellus blandit. Etiam nec odio vestibul. Etiam nec odio vestibulum est mat tis effic iturut magna. Pellentesque sit amet tellus blandit. Etiam nec odio ves tibul. Nec odio vestibulum est mattis effic iturut magna. Pellente sque sita met tellus blandit. Etiam nec odio. Etiam nec odio vestibulum est mattis effic iturut magna. Pellentesque sit am et tellus blandit. Etiam nec odio vestibul. Etiam nec odio vestibulum est mat tis effic iturut magna.\r\n\r\nEtiam nec odio vestibulum est mattis effic iturut magna. Pellentesque sit am et tellus blandit. Etiam nec odio vestibul. Etiam nec odio vestibulum est mat tis effic iturut magna. Pellentesque sit amet tellus blandit. Etiam nec odio ves tibul. Nec odio vestibulum est mattis effic iturut magna. Pellente sque sita met tellus blandit. Etiam nec odio. Etiam nec odio vestibulum est mattis effic iturut magna. Pellentesque sit am et tellus blandit. Etiam nec odio vestibul. Etiam nec odio vestibulum est mat tis effic iturut magna.\r\n\r\nEtiam nec odio vestibulum est mattis effic iturut magna. Pellentesque sit am et tellus blandit. Etiam nec odio vestibul. Etiam nec odio vestibulum est mat tis effic iturut magna. Pellentesque sit amet tellus blandit. Etiam nec odio ves tibul. Nec odio vestibulum est mattis effic iturut magna. Pellente sque sita met tellus blandit. Etiam nec odio. Etiam nec odio vestibulum est mattis effic iturut magna. Pellentesque sit am et tellus blandit. Etiam nec odio vestibul. Etiam nec odio vestibulum est mat tis effic iturut magna.\r\n\r\nEtiam nec odio vestibulum est mattis effic iturut magna. Pellentesque sit am et tellus blandit. Etiam nec odio vestibul. Etiam nec odio vestibulum est mat tis effic iturut magna. Pellentesque sit amet tellus blandit. Etiam nec odio ves tibul. Nec odio vestibulum est mattis effic iturut magna. Pellente sque sita met tellus blandit. Etiam nec odio. Etiam nec odio vestibulum est mattis effic iturut magna. Pellentesque sit am et tellus blandit. Etiam nec odio vestibul. Etiam nec odio vestibulum est mat tis effic iturut magna.\r\n\r\nEtiam nec odio vestibulum est mattis effic iturut magna. Pellentesque sit am et tellus blandit. Etiam nec odio vestibul. Etiam nec odio vestibulum est mat tis effic iturut magna. Pellentesque sit amet tellus blandit. Etiam nec odio ves tibul. Nec odio vestibulum est mattis effic iturut magna. Pellente sque sita met tellus blandit. Etiam nec odio. Etiam nec odio vestibulum est mattis effic iturut magna. Pellentesque sit am et tellus blandit. Etiam nec odio vestibul. Etiam nec odio vestibulum est mat tis effic iturut magna.\r\n\r\nEtiam nec odio vestibulum est mattis effic iturut magna. Pellentesque sit am et tellus blandit. Etiam nec odio vestibul. Etiam nec odio vestibulum est mat tis effic iturut magna. Pellentesque sit amet tellus blandit. Etiam nec odio ves tibul. Nec odio vestibulum est mattis effic iturut magna. Pellente sque sita met tellus blandit. Etiam nec odio. Etiam nec odio vestibulum est mattis effic iturut magna. Pellentesque sit am et tellus blandit. Etiam nec odio vestibul. Etiam nec odio vestibulum est mat tis effic iturut magna.\r\n\r\nEtiam nec odio vestibulum est mattis effic iturut magna. Pellentesque sit am et tellus blandit. Etiam nec odio vestibul. Etiam nec odio vestibulum est mat tis effic iturut magna. Pellentesque sit amet tellus blandit. Etiam nec odio ves tibul. Nec odio vestibulum est mattis effic iturut magna. Pellente sque sita met tellus blandit. Etiam nec odio. Etiam nec odio vestibulum est mattis effic iturut magna. Pellentesque sit am et tellus blandit. Etiam nec odio vestibul. Etiam nec odio vestibulum est mat tis effic iturut magna.\r\n\r\nEtiam nec odio vestibulum est mattis effic iturut magna. Pellentesque sit am et tellus blandit. Etiam nec odio vestibul. Etiam nec odio vestibulum est mat tis effic iturut magna. Pellentesque sit amet tellus blandit. Etiam nec odio ves tibul. Nec odio vestibulum est mattis effic iturut magna. Pellente sque sita met tellus blandit. Etiam nec odio. Etiam nec odio vestibulum est mattis effic iturut magna. Pellentesque sit am et tellus blandit. Etiam nec odio vestibul. Etiam nec odio vestibulum est mat tis effic iturut magna.\r\n\r\nEtiam nec odio vestibulum est mattis effic iturut magna. Pellentesque sit am et tellus blandit. Etiam nec odio vestibul. Etiam nec odio vestibulum est mat tis effic iturut magna. Pellentesque sit amet tellus blandit. Etiam nec odio ves tibul. Nec odio vestibulum est mattis effic iturut magna. Pellente sque sita met tellus blandit. Etiam nec odio. Etiam nec odio vestibulum est mattis effic iturut magna. Pellentesque sit am et tellus blandit. Etiam nec odio vestibul. Etiam nec odio vestibulum est mat tis effic iturut magna.\r\n\r\nEtiam nec odio vestibulum est mattis effic iturut magna. Pellentesque sit am et tellus blandit. Etiam nec odio vestibul. Etiam nec odio vestibulum est mat tis effic iturut magna. Pellentesque sit amet tellus blandit. Etiam nec odio ves tibul. Nec odio vestibulum est mattis effic iturut magna. Pellente sque sita met tellus blandit. Etiam nec odio. Etiam nec odio vestibulum est mattis effic iturut magna. Pellentesque sit am et tellus blandit. Etiam nec odio vestibul. Etiam nec odio vestibulum est mat tis effic iturut magna.\r\n\r\nEtiam nec odio vestibulum est mattis effic iturut magna. Pellentesque sit am et tellus blandit. Etiam nec odio vestibul. Etiam nec odio vestibulum est mat tis effic iturut magna. Pellentesque sit amet tellus blandit. Etiam nec odio ves tibul. Nec odio vestibulum est mattis effic iturut magna. Pellente sque sita met tellus blandit. Etiam nec odio. Etiam nec odio vestibulum est mattis effic iturut magna. Pellentesque sit am et tellus blandit. Etiam nec odio vestibul. Etiam nec odio vestibulum est mat tis effic iturut magna.\r\n\r\nEtiam nec odio vestibulum est mattis effic iturut magna. Pellentesque sit am et tellus blandit. Etiam nec odio vestibul. Etiam nec odio vestibulum est mat tis effic iturut magna. Pellentesque sit amet tellus blandit. Etiam nec odio ves tibul. Nec odio vestibulum est mattis effic iturut magna. Pellente sque sita met tellus blandit. Etiam nec odio. Etiam nec odio vestibulum est mattis effic iturut magna. Pellentesque sit am et tellus blandit. Etiam nec odio vestibul. Etiam nec odio vestibulum est mat tis effic iturut magna.\r\n\r\n'),
(2, '\r\n', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `unique_visits`
--

DROP TABLE IF EXISTS `unique_visits`;
CREATE TABLE `unique_visits` (
  `fromIP` text NOT NULL,
  `agent` text NOT NULL,
  `country` text,
  `city` text,
  `timeVisited` varchar(20) NOT NULL,
  `referrer` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `ID` int(10) NOT NULL,
  `firstname` varchar(20) NOT NULL,
  `lastname` varchar(20) NOT NULL,
  `email` varchar(80) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(255) NOT NULL,
  `dateCreated` varchar(60) NOT NULL,
  `accType` enum('admin','normal','','') NOT NULL DEFAULT 'normal'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`ID`, `firstname`, `lastname`, `email`, `username`, `password`, `dateCreated`, `accType`) VALUES
(1, 'admin', 'admin', 'admin@mytodo.com', 'Admin', '$2y$11$I0eE9KwJQ5wkA4tYVUily.sEDEZfxTIDZuinaM7O8SFwqv/bf0/9W', '2018 Oct Fri 01:18:56', ''),
(3, 'Blancos', 'Khim', 'briansiranga@gmail.com', 'blancos khim', '$2y$11$cs/aIDGotUCRQMvTYSeXTuw0G.FvfptcOZjpxCQ96HTpOjPD5Hkue', '2018 Oct Thu 12:55:46', ''),
(4, 'asex', 'shiru', 'alexaj@ea.com', 'alexis', '$2y$12$aYg.dY/SLw8KhPblJ58VOearkTh/yOXTAZTOSyyp0.Sdk7nmctPZe', '2018 Oct Fri 13:21:52', 'normal'),
(5, 'patrick', 'wanjiru', 'kinyua113@gmail.com', 'pk', '$2y$12$QnKjULsrnNYHaT80R5Bjw.6Kiww/93De6qJuIxoKCpyZKYDtXriOm', '2018 Nov Fri 16:59:36', 'normal');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `adverts`
--
ALTER TABLE `adverts`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `crimes`
--
ALTER TABLE `crimes`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `postedByID` (`postedByID`);

--
-- Indexes for table `locations`
--
ALTER TABLE `locations`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `ID` (`ID`),
  ADD KEY `ID_2` (`ID`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `adverts`
--
ALTER TABLE `adverts`
  MODIFY `ID` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `crimes`
--
ALTER TABLE `crimes`
  MODIFY `ID` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=171;

--
-- AUTO_INCREMENT for table `locations`
--
ALTER TABLE `locations`
  MODIFY `ID` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `ID` int(13) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `ID` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `crimes`
--
ALTER TABLE `crimes`
  ADD CONSTRAINT `crimes_ibfk_1` FOREIGN KEY (`postedByID`) REFERENCES `users` (`ID`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
