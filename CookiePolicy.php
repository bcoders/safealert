<?php
include_once 'inc/header.php';
?>
<style type="text/css">
a{
	text-decoration: underline;
}
.footer a{
	text-decoration: none;
}
.footer-bottom a{
	text-decoration: none;
}
p{
	font-family: serif;
	line-height: 1.5;
}
.row .cookie-policy{
	margin-left: 260px;
}
	.policy-header{
		font-family: sans-serif;
		font-weight: bold;
		margin-left: 10px;
	}
	.content-header{
		font-weight: bold;
	}
</style>
<div class="row">
<div class="cookie-policy">
			<div class="policy-header" style="font-size:150%;text-align:center;">Cookie Policy</div>
		<div class="col-md-12">
<div class='content-header' id="introduction">Introduction</div>
<p>
	Our website uses cookies.
	Insofar as those cookies are not strictly necessary for the provision of our website and services, we will ask you to consent to our use of cookies when you first visit our website.
</p>
<div class="content-header" id="about_cookies">About cookies</div>
<p>
	A <a href="https://en.wikipedia.org/wiki/HTTP_cookie" target="_blank">cookie</a> is a file containing an identifier (a string of letters and numbers) that is sent by a web server to a web browser and is stored by the browser. The identifier is then sent back to the server each time the browser requests a page from the server.<br/>
	Cookies may be either <a href="https://en.wikipedia.org/wiki/HTTP_cookie#Persistent_cookie">"persistent" cookies</a> or <a href="
	https://en.wikipedia.org/wiki/HTTP_cookie#Session_cookie">"session" cookies</a>: a persistent cookie will be stored by a web browser and will remain valid until its set expiry date, unless deleted by the user before the expiry date; a session cookie, on the other hand, will expire at the	end of the user session, when the web browser is closed.
</p>
<p>
	Cookies do not typically contain any information that personally identifies a user, but personal information that we store about you may be linked to the information stored in and obtained from cookies.
</p>

<div class="content-header">Cookies that we use</div>
<p>
	We use cookies for the following purposes:
	<ul class="cookie-fns">
		<li><b>Authentication</b> - we use cookies to identify you when you visit our website and as you navigate our website</li>
		<li><b>Status</b> - we use cookies to help us to determine if you are logged into our website</li>
		<li><b>Personalisation</b> - we use cookies to store information about your preferences and to personalise our website for you</li>
		<li><b>Security</b> - we use cookies as an element of the security measures used to protect user accounts, including preventing fraudulent use of login credentials, and to protect our website and services generally</li>
		<li><b>Advertising</b> - we may use cookies to help us to display advertisements that will be relevant to you</li>
		<li><b>Analysis</b> - we use cookies to help us to analyse the use and performance of our website and services</li>
		<li><b>Cookie consent</b> - we use cookies to store your preferences in relation to the use of cookies more generally. </li>
	</ul>

</p>
<div class="content-header">Cookies used by our service providers</div>
<p>
Our service providers use cookies and those cookies may be stored on your computer when you visit our website.<br/>
&ensp;&ensp;&ensp;&ensp;We use Google Analytics to analyse the use of our website. Google Analytics gathers information about website use by means of cookies. The information gathered relating to our website is used to create reports about the use of our website. Google's privacy policy is available at: https://www.google.com/policies/privacy/.<br/>
&ensp;&ensp;&ensp;&ensp;	We may publish Google AdSense and other interest-based advertisements on our website. These are tailored to reflect your interests. To determine your interests, Google will track your behaviour on our website and on other websites across the web using cookies. To determine your interests, Google will track your behaviour on our website and on other websites across the web using cookies.<br/>
&ensp;&ensp;&ensp;&ensp; This behaviour tracking allows Google to tailor the advertisements that you see on other websites to reflect your interests (but we do not publish interest-based advertisements on our website).] You can view, delete or add interest categories associated with your browser by visiting: https://adssettings.google.com. You can also opt out of the AdSense partner network cookie using those settings or using the Network Advertising Initiative's multi-cookie opt-out mechanism at: http://optout.networkadvertising.org. However, these opt-out mechanisms themselves use cookies, and if you clear the cookies from your browser your opt-out will not be maintained. To ensure that an opt-out is maintained in respect of a particular browser, you may wish to consider using the Google browser plug-ins available at: https://support.google.com/ads/answer/7395996.
</p>
<div class="content-header">Managing Cookies</div>
<p>
		Most browsers allow you to refuse to accept cookies and to delete cookies. The methods for doing so vary from browser to browser, and from version to version. You can however obtain up-to-date information about blocking and deleting cookies via these links:
		<ol style="list-style-type: lower-alpha;">
			<li><a href="https://support.google.com/chrome/answer/95647?hl=en">https://support.google.com/chrome/answer/95647?hl=en </a> - (Chrome);</li>
			<li><a href="https://support.mozilla.org/en-US/kb/enable-and-disable-cookies-website-preferences">https://support.mozilla.org/en-US/kb/enable-and-disable-cookies-website-preferences</a> - (Firefox)</li>
			<li><a href="http://www.opera.com/help/tutorials/security/cookies/">http://www.opera.com/help/tutorials/security/cookies/ </a>(Opera)</li>
			<li><a href="https://support.microsoft.com/en-gb/help/17442/windows-internet-explorer-delete-manage-cookies">https://support.microsoft.com/en-gb/help/17442/windows-internet-explorer-delete-manage-cookies</a> - (Internet Explorer)</li>
			<li><a href="https://support.apple.com/kb/PH21411">https://support.apple.com/kb/PH21411</a> - (Safari)</li>
			<li><a href="https://privacy.microsoft.com/en-us/windows-10-microsoft-edge-and-privacy">https://privacy.microsoft.com/en-us/windows-10-microsoft-edge-and-privacy</a> - (Edge)</li>
		</ol>
</p>
<div class="col-md-6">
	<span class="content-header">Contact Us</span>
	<ol style="">
	<li>using our website contact form</li>	
	<li>by telephone, or the contact number published on our website from time to time</li>
	<li>by email, using the email address published on our website from time to time</li>
	</ol>
</div>
</div>
<p style="margin-left:15px;">
	Blocking all cookies will have a negative impact upon the usability of many websites.<br/>
	If you block cookies, you will not be able to use all the features on our website.
</p>
	</div>
	

</div>
</div>
<?php
include_once 'inc/footer.php';
