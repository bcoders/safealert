<?php
session_start();
ob_start();
/**
 * Created by PhpStorm.
 * User: Coder
 * Date: 2/21/2018
 * Time: 6:09 AM
 */

// we init all our settings first
require_once 'sessions/settings.php';
require_once 'sessions/functions.php';

/**
 * save unique visits
 */
require_once 'saveuniquevisits.php';

/**
 * This is the header file that is used all over my project
 * it contains the top (header) of the site
 */
if (!empty($_SERVER['HTTPS']) && ('on' == $_SERVER['HTTPS'])) {
    $uri = 'https://';
} else {
    $uri = 'http://';
}
$uri .= $_SERVER['HTTP_HOST'];
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" type="text/css" href="assets/font-awesome/css/font-awesome.min.css"/>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <base href="<?php echo $uri . '/' . _PROJECT . '/'; ?>">
    <title><?php
        if (isset($pageName))
        {
            echo "MyToDo - " . $pageName;
        }
        else
        {
            echo "My ToDo Site" ;
        }
        ?></title>
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="assets/font-awesome/css/font-awesome.min.css"/>
        <link rel="stylesheet" href="assets/css/form-elements.css">
        <link rel="stylesheet" href="assets/css/MTDFormstyle.css">
        <link rel="stylesheet" href="assets/css/style2.css">
        <link rel="stylesheet" href="assets/css/jquery.mCustomScrollbar.min.css">
        <script src ="assets/js/solid.js"></script>
        <script src="assets/js/fontawesome.js"></script>

    <script type="text/javascript" src="assets/js/jquery.js"></script>
    <script type = "text/javascript" src="assets/js/jquery-ui/jquery-ui.js"></script>
    <!-- custom JavaScript -->
    <script type = "text/javascript" src="assets/js/MyJs.js"></script>
    <!--[if IE 7]>
    <link rel="stylesheet" href="assets/font-awesome/css/font-awesome-ie7.min.css"/>
    <![endif]-->
    <link rel="stylesheet" href="assets/css/MyToDo.css"/>
    <script src='https://www.google.com/recaptcha/api.js'></script>
    <link rel="stylesheet" href="assets/css/MediaQueries.css"/>
    <style>
        html, body, h1, h2, h3, h4, h5, h6, p, blockquote, pre, img {
            margin: 0;
            padding: 0;
            border: 0px none currentcolor;
            border-image-outset: 0;
            border-image-repeat: stretch;
            border-image-slice: 100%;
            border-image-source: none;border-image-width: 1;
            font-size: 100%;
            font: inherit;
            font-style: inherit;
            font-variant-caps: inherit;
            font-weight: inherit;
            font-stretch: inherit;
            font-size: inherit;
            line-height: inherit;
            font-family: inherit;
            font-size-adjust: inherit;
            font-kerning: inherit;
            font-optical-sizing: ;
            font-variant-alternates: inherit;
            font-variant-east-asian: inherit;
            font-variant-ligatures: inherit;
            font-variant-numeric: inherit;
            font-variant-position: inherit;
            font-language-override: inherit;
            font-feature-settings: inherit;
            font-variation-settings: ;
            font-size: inherit;
            line-height: inherit;
            font-family: inherit;
            vertical-align: baseline;
        }
</style>
</head>
<body>
<div id="cookie-message">
        We use cookies on this website. By continuing to use our site you agree to our <a href='CookiePolicy.php'>cookie policy</a>
</div>
<script>
    // Detect JS support
    document.body.className = document.body.className + " js_enabled";
</script>
<div class="main-header">
<div class="social-icons">
    <ul class="list-unstyled">
        <li><i class="fa fa-facebook-square"></i> </li>
    </ul>
</div>
</div>
<div class="sticky">
    <div class="nav top-menu">
        <div class="wrapper">
            <ol class="list-unstyled list-inline accounts-menu" style="color:#FFFFFF;">
                <li><a href="index.php"><i class="fa fa-home"></i> Home</a> </li>
                <li><a href="About.php">About</a> </li>
                <li><a href="#"><i class=""></i> Contact</a></li>
                <?php
                // if user is logged in show a different menu from those not logged in
                if (isset($_SESSION['loggedInToUs'])):
                    ?><li class=""> <a class="" href="MyAccount.php">My Account</a> </li>
                    <li> <a href="LogOut.php">Logout</a> </li>
                <?php
                else:
                    ?><li> <a href="SignIn.php" class=""> Login</a> </li>
                    <li><a href="CreateAccount.php" class=""><i class="fa fa-registered"></i> Register</a> </li>
                <?php
                endif;
                ?>
            </ol>
        </div>
    </div>
</div>



<div class="container-fluid">
    <div class="row">
        <div class="col-md-2">
            <?php
            include_once "inc/LeftSidebar.inc";
            ?>
        </div>
