<?php
/**
 * Created by PhpStorm.
 * User: Coder
 * Date: 11/3/2018
 * Time: 7:40 AM
 */

/*
 * Show all errors (not required of course)
 */
ini_set('display_errors','Off');
//error_reporting(-1);


/*
 * Include the pagination.php class file
 */
require_once('sessions/Pagination.php');

/**
 * do something with our db file
 */
include_once "sessions/Db.class.php";


/*
 * Connect to the database (Replacing the XXXXXX's with the correct details)
 */
try
{
    $dbh = new PDO('mysql:host=localhost;dbname=krime', 'blancos', 'blancoskhim');
    $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
}
catch(PDOException $e)
{
    print "Error!: " . $e->getMessage() . "<br/>";
}


/*
 * Get and/or set the page number we are on
 */
if(isset($_GET['page']))
{
    $page = $_GET['page'];
}
else
{
    $page = 1;
}


/*
 * Set a few of the basic options for the class, replacing the URL with your own of course
 */
$cDate = $_GET['sr-date'];
$options = array(
    'results_per_page' => 2,
    'url' => "http://localhost/ToDo/Crime.php?sr-location=&sr-type=&sr-date=$cDate&sr-results=Submit&page=*VAR*",
    'db_handle' => $dbh
);


/*
 * Create the pagination object
 */
try
{
    $paginate = new pagination($page, "SELECT * FROM crimes WHERE `dateOccurred` LIKE '%{$cDate}%' ORDER BY ID DESC ", $options);
}
catch(paginationException $e)
{
    echo $e;
    exit();
}

/*
 * If we get a success, carry on
 */
if($paginate->success == true)
{

    /*
     * Fetch our results
     */
    $result = $paginate->resultset->fetchAll();


    /*
     * Work with our data rows
     */
    foreach($result as $sDate):

        // get the userID and get the username of the user who posted the crime
        $userName = $functions->GetUserNameByID($sDate['postedByID']);
        $postingdate  = $functions->GetCrimePostingDate($sDate['ID']);
        ?>
        <div class="col-md-9">
            <div class="panel panel-info">
                <div class="panel-heading">
                    A <?php echo '<b class="slug">' . ucfirst($sDate['slug']) . '</b>' . ' crime committed on <b class="time">' . $sDate['dateOccurred'] . '</b><br/>';
                    echo 'The crime occurred at <b class="location">' . $sDate['location'] . '</b>';
                    ?>
                </div>
                <div class="panel-body">
                    <?php
                    $fullDescription = $sDate['description'];
                    if (strlen($fullDescription) > 699)
                    {
                        $pos = strpos($fullDescription, ' ', 700);
                        echo '<p>' . substr($fullDescription, 0, $pos) . '......</p>';?>
                        <a href="Crime.php?CiD=<?php echo base64_encode($sDate['ID']); ?>&description=<?php echo base64_encode($sDate['slug']); ?>&occt=<?php echo base64_encode($sDate['timeOccurred']);?>" class='btn btn-info'> View Crime</a>
                        <?php
                    }
                    else
                    {
                        echo $fullDescription;
                    }
                    ?>
                </div>
            </div>
        </div>

    <?php
    endforeach;
    /*
        * Echo out the UL with the page links
        */
    echo '<p>'.$paginate->links_html.'</p>';


}
?>