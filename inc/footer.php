<?php
/**
 * Created by PhpStorm.
 * User: Coder
 * Date: 2/21/2018
 * Time: 6:09 AM
 */
/**
 * this is the common footer file for all the files
 */
?>
<!-- closing the container fluid first -->
</div>
</div>
<div class="footer container-fluid">
    <div class="row">
        <div class="col-md-3">
            <h5>Krime</h5>
            Blancorp&trade; is your crime update website. We provide you with the latest crime news and videos straight from the dark world.
        </div>
        <div class="col-md-3" >
            <h5>Quick Links</h5>
            <ol class="list-unstyled">
                <li><a href="#">Home</a> </li>
                <li><a href="About.php">About</a> </li>
                <li><a href="#">Services</a> </li>
                <li><a href="#">Contact</a> </li>
                <li><a href="#">API</a> </li>
                <li><a href="#">Documentation</a></li>
            </ol>
        </div>

        <div class="col-md-3" >
            <h5>Blog</h5>

        </div>

        <div class="col-md-3" style="padding-bottom: 15px">
            <h5>Contact Info</h5>
                <p><i class="fa fa-map-pin"></i>  4127/ 5B-C Mislane Road, Gibraltar, UK</p>
            <p><i class="fa fa-phone"></i>  Main: 203-808-8613 <br> Office: 203-808-8648</p>
            <p><a href="mailto:contact@kriminoo.com">contact@kriminoo.com</a></p>
            </div>
        </div>
    </div>

        <div class="row footer-bottom">
            <div class="col-md-4 text-left header">
                &copy; Krime <?php echo date('Y');?>&ensp;- Keep Updated
            </div>
            <div class="col-md-8 text-right">
                <ul class="list-inline">
                    <li><a href="PrivacyPolicy.php">Privacy Policy</a> </li>
                    <li><a href="CookiePolicy.php">Cookie Policy</a></li>
                    <li><a href="Review.php">Write A Review</a></li>
                    <li><a href="TnC.php">Terms &amp; Conditions</a></li>
                </ul>
            </div>

<div class="col-md-12 licence">
	<a rel="license" href="http://creativecommons.org/licenses/by/2.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/2.0/88x31.png" /></a><br />Content on this website is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by/2.0/">Creative Commons Attribution 2.0 Generic License</a>.
</div>
</div>
<!-- jQuery CDN - Slim version (=without AJAX) -->
<!-- <script src="assets/js/jquery-3.3.1.slim.min.js" crossorigin="anonymous"></script> -->
<!-- Popper.JS -->
<!--<script src="assets/js/popper.min.js" crossorigin="anonymous"></script> -->
<!-- Bootstrap JS -->
<script src="assets/js/bootstrap.min.js" crossorigin="anonymous"></script>
<!-- jQuery Custom Scroller CDN -->
<script src="assets/js/jquery.mCustomScrollbar.concat.min.js"></script>
<!-- cookie script -->
<script src="assets/js/cookie-message.js"></script>
<script type="text/javascript">
    $(document).ready(function () {

        $("#sidebar").mCustomScrollbar({
            theme: "minimal"
        });

        $('#sidebarCollapse').on('click', function () {
            // open or close navbar
            $('#sidebar').toggleClass('active');
            // close dropdowns
            $('.collapse.in').toggleClass('in');
            // and also adjust aria-expanded attributes we use for the open/closed arrows
            // in our CSS
            $('a[aria-expanded=true]').attr('aria-expanded', 'false');
        });

    });
</script>
</div>
</html>
