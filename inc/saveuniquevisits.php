<?php
/**
 * Created by PhpStorm.
 * User: Coder
 * Date: 12/9/2018
 * Time: 7:41 PM
 */


require_once 'sessions/Db.class.php';
if (isset($_COOKIE['seen-cookie-message'])){
    # the user is not uniques so we just ignore them
    //echo 'Hey I\'m here<br/>';
    //echo $_SERVER['HTTP_ACCEPT'];
}
else{
    /**
     * the user is new so we log their details
     * according to EU rules, we can't save plain IP addresses  to db
     * we has them
     **/
    //echo "Sorry!! I don't exist<br/><br/>";
    $from = $_SERVER['REMOTE_ADDR'];
    if (isset($_SERVER['HTTP_REFERER'])){
        $referrer = $_SERVER['HTTP_REFERER'];
    }else{
        $referrer = NULL;
    }
    $time = date('H:s:i d-m-Y, D');
    $agent = $_SERVER["HTTP_USER_AGENT"];

    //echo $from . $agent . $time . $referrer;

    try{
        $db->beginTransaction();
        $db->query("INSERT INTO `unique_visits` (`fromIP`,`agent`,`timeVisited`,`referrer`) VALUES (:ip,:agent,:tym,:referrer)", array('ip'=>$from,
            'agent'=>$agent,
            'tym'=>$time,
            'referrer'=>$referrer,
    ));
        $db->executeTransaction();
    }
    catch (Exception $e){
        $db->rollBack();
    }

}