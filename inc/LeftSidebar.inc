
    <nav id="sidebar">
        <div class="sticky">
            <div class="sidebar-header">
                <h3 class="sr-only">Search</h3>
            </div>
            <div class="crime-search-form">
                <form class="" method="get" action="Crime.php" autocomplete="off">
                    <div class="form-group">
                        <label for="sr-location">Search by Location</label>
                        <input class="form-control" type="text" name="sr-location" id="sr-location" placeholder="Kasarani"/>
                    </div>

                    <div class="form-group">
                        <label for="sr-type">Search by type</label>
                        <input class="form-control" type="text" name="sr-type" id="sr-type" placeholder="Burglary/Kidnapping" value=""/>
                    </div>

                    <div class="form-group">
                        <label for="sr-type">Search by date</label>
                        <input class="form-control" type="date" name="sr-date" id="sr-date" placeholder="MM/DD/YYYY"/>
                    </div>

                    <input type="submit" name="sr-results" class="btn btn-primary" value="Submit"/>
                </form>
            </div>
        </div>
    </nav>

