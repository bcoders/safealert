<?php
/**
 * Created by PhpStorm.
 * User: Coder
 * Date: 11/13/2018
 * Time: 6:50 PM
 * @param $fullDescription
 * @param $slug
 * @param $postingdate
 * @param $crimeID
 * @param $timeOccured
 */

function GetCrimeList($fullDescription, $slug, $postingdate, $crimeID, $timeOccured){
    ?>
    <div class="panel panel-info index-crimes" id="crime-list">
        <div class="panel-heading text-right top-header">
            <i class="fa fa-bullhorn"></i>&nbsp;Posted <?php echo /*'<b>' . ucwords($userName) . '</b>*/' on <b>' . $postingdate . '</b>';?>
        </div>
        <div class="panel-heading bottom-header">
            <?php echo $slug;?>
        </div>
        <div class="panel-body">
            <?php
            if (strlen($fullDescription) > 700):
                $pos = strpos($fullDescription, ' ', 699);
                $trimmedDescr = substr($fullDescription, 0, $pos);
                $crimeID = base64_encode($crimeID);
                $slug= base64_encode($slug);
                $timeOccured = base64_encode($timeOccured);

                echo <<<EOT
 <p> . $trimmedDescr . ......</p>
                <a href="Crime.php?CiD=$crimeID&description=$slug&occt=$timeOccured" class="btn btn-info"> Explore Crime</a>
EOT;

            else:
            $crimeID = base64_encode($crimeID);
            $slug= base64_encode($slug);
            $timeOccured = base64_encode($timeOccured);

            echo <<<EOT
 $fullDescription . <br/> 
                <a href="Crime.php?CiD=$crimeID&description=$slug&occt=$timeOccured" class="btn btn-info"> Explore Crime</a>
EOT;

           endif;
            ?>
        </div>
    </div>
    
<?php
}