<?php
/**
 * Created by PhpStorm.
 * User: Coder
 * Date: 2/26/2018
 * Time: 3:38 AM
 */
$pageName = "Privacy Policy";
require_once "inc/header.php";
?>
<style type="text/css">
a{
	text-decoration: underline;
}
.footer a{
	text-decoration: none;
}
.footer-bottom a{
	text-decoration: none;
}
p{
	font-family: serif;
	line-height: 1.5;
}
.row .tnc{
	margin-left: 260px;
}
	.policy-header{
		font-family: sans-serif;
		font-weight: bold;
		margin-left: 10px;
	}
	.content-header{
		font-weight: bold;
		font-size: 150%;
		margin: 30px 0 25px 0;
	}
	.sub-header{
		font-weight: bold;
		font-size: 110%;
		margin: 10px 0 3px 0;
	}

}
</style>
<div class="row">
	<div class="tnc">
		<div class="col-md-12">
		<div class="policy-header" style="font-size:150%;text-align:center;">Terms and Conditions of Use</div>
	<div class="content-header" id="introduction">Introduction</div>
	<p class="introduction">
		These terms and conditions shall govern your use of our website.<br/>
	By using our website, you accept these terms and conditions in full; accordingly, if you disagree with these terms and conditions or any part of these terms and conditions, you must not use our website.<br/>
	If you register with our website, submit any material to our website or use any of our website services, we will ask you to expressly agree to these terms and conditions.<br/>
	Our website uses cookies; by using our website or agreeing to these terms and conditions, you consent to our use of cookies in accordance with the terms of our <a href="PrivacyPolicy.php">Privacy Policy</a> and <a href="CookiePolicy.php">Cookie Policy</a>.
	</p>
	
	<div class="content-header">Copyright notice</div>
	<p class="copyright">
		Copyright &copy; 2018 of first publication.<br/>
		Subject to the express provisions of these terms and conditions:
		<ul style="list-style-type: square">
			<li>we, together with our licensors, own and control all the copyright and other intellectual property rights in our website and the material on our website; and</li>
			<li>all the copyright and other intellectual property rights in our website and the material on our website are reserved.</li>
		</ul>
	</p>

<div class="content-header" id="licence-to-use">Licence to use website</div>
<b>
	Remember that all content published (including content posted by users) on this website is under and inherits the terms explained under the <a rel="license" href="http://creativecommons.org/licenses/by/2.0/">Creative Commons Licence</a> unless where explicitly stated otherwise.
</b>
<p>
	Content posted, published on our website will inherit the terms of the above stated licence unless where explicitly stated otherwise..<br/> Aggreeing to register, log 	in or use our website will be in terms with the Creative Commons Licence.</p>
<p class="licence-to-use">
	You may:
	<ul style="list-style-type: square">
		<li>view pages from our website in a web browser;</li>
		<li>download pages from our website for caching in a web browser;</li>
		<li>print pages from our website;</li>
		<li>stream audio and video files from our website; and</li>
		<li>use our website services by means of a web browser, subject to the other provisions of these terms and conditions.</li>
	</ul>

	<p>Our content is governed by the Creative Commons Licence, and unless where explicitly stated, you may:</p>
	<ul style="list-style-type: square">
	<li>republish material from our website including republication on another website;</li>
	<li>sell, rent or sub-license material from our website;</li>
	<li>show any material from our website in public;</li>
	<li>exploit material from our website for a commercial purpose; or</li>
	<li>redistribute material from our website.</li>
	as stated and defined under the <a rel="license" href="http://creativecommons.org/licenses/by/2.0/">Creative Commons Licence</a>
	</ul>
	<p>
	</p>
	<p>You may redistribute our newsletter in print and/or electronic form to any person.<br/>
	We reserve the right to restrict access to areas of our website, or indeed our whole website, at our discretion; you must not circumvent 	or bypass, or attempt to circumvent or bypass, any access restriction measures on our website.</p>
</p>

<div class="content-header" id="acceptable-use">Acceptable Use</div>
<p class="acceptable-use">
You must not:
<ul style="list-style-type: square">
<li>use our website in any way or take any action that causes, or may cause, damage to the website or impairment of the performance, availability or accessibility of the website;</li>	
<li>use our website in any way that is unlawful, illegal, fraudulent or harmful, or in connection with any unlawful, illegal, fraudulent or harmful purpose or activity;</li>
<li>use our website to copy, store, host, transmit, send, use, publish or distribute any material which consists of (or is linked to) any spyware, computer virus, Trojan horse, worm, keystroke logger, rootkit or other malicious computer software;</li>
<li>conduct any systematic or automated data collection activities (including without limitation scraping, data mining, data extraction and data harvesting) on or in relation to our website without our express written consent;</li>
<li>access or otherwise interact with our website using any robot, spider or other automated means, except for the purpose of search engine indexing;</li>
<li>violate the directives set out in the robots.txt file for our website;</li>
<li>use data collected from our website for any direct marketing activity (including without limitation, email marketing, SMS marketing, telemarketing and direct mailing)</li>
</ul>
You must not use data collected from our website to contact individuals, companies or other persons or entities.<br/>
You must ensure that all the information you supply to us through our website, or in relation to our website, is true, accurate, current, complete and non-misleading.
</p>

<div class="content-header" id="accounts">Registration and Accounts</div>
<p class="reg-and-acc">
	To be eligible for an account on our website under this Section, you must be resident or situated in Kenya.<br/>
	You may register for an account with our website by completing and submitting the account registration form on our website, and clicking on the verification link in the email that the website will send to you.<br/>
	You must not allow any other person to use your account to access the website.<br/>
	You must notify us in writing or via our support email immediately if you become aware of any unauthorised use of your account.<br/>
	You must not use any other person's account to access the website, unless you have that person's express permission to do so.
</p>
		
	<div class="sub-header">Login Details</div>
<p>
	If you register for an account with our website, you will be asked to choose a username ID and password.<br/>
	Your user ID must not be liable to mislead and must comply with the content rules set out in <a href="TnC.php#content-rules">content rules</a>  section; <b>you must not use your account or user ID for or in connection with the impersonation of any person.</b><br/> 
	You must keep your password confidential.<br/>
	You must notify us in writing or our support email immediately if you become aware of any disclosure of your password.<br/>
	You are responsible for any activity on our website arising out of any failure to keep your password confidential, and may be held liable for any losses arising out of such a failure.
</p>

<div class="content-header" id="account-cancellation">Cancellation and Suspension of Account</div>
	<p>
	We may:
		<ul style="list-style-type: square">
		<li>Suspend your account;</li>
		<li>Cancel your account; and/or</li>
		<li>Edit your account details</li>
	</ul>
	...at any time in our sole discretion without notice or explanation.<br/><br/>
	You may cancel your account on our website using your account control panel on the website.
	</p>

<div class="content-header" id="content-licence">Your content: licence</div>
<p class="content-licence">
In these terms and conditions, "your content" means all works and materials (including without limitation text, graphics, images, audio material, video material, audio-visual material, scripts, software and files) that you submit to us or our website for storage or publication on, processing by, or transmission via, our website.

	<ul style="list-style-type: square">

	<li>	You grant to us a licence to use, reproduce, store and publish your content on and in relation to this website and any successor website.</li>
	<li>You grant to us the right to sub-license the rights licensed under <a href="TnC.php#licence-to-use">licence terms</a> section.</li>
	<li>You grant to us the right to bring an action for infringement of the rights licensed under <a href="TnC.php#licence-to-use">licence terms</a> section.</li>
	<li>You hereby waive all your moral rights in your content to the maximum extent permitted by applicable law; and you warrant and represent that all other moral rights in your content have been waived to the maximum extent permitted by applicable law.</li>
	<li>You may edit your content to the extent permitted using the editing functionality made available on our website.</li>
	<li>Without prejudice to our other rights under these terms and conditions, if you breach any provision of these terms and conditions in any way, or if we reasonably suspect that you have breached these terms and conditions in any way, we may delete, unpublish or edit any or all of your content.</li>
	</ul>
</p>

<div class="content-header" id="content-rules">Your content: rules</div>
<p class="content-rules">
	You warrant and represent that your content will comply with these terms and conditions.<br/>
	Your content must not be illegal or unlawful, must not infringe any person's legal rights, and must not be capable of giving rise to legal action against any person (in each case in any jurisdiction and under any applicable law).<br/><br/>
			Your content, and the use of your content by us in accordance with these terms and conditions, must not:
	<ul style="list-style-type: square">
		<li>be libellous or maliciously false;</li>
		<li>be obscene or indecent;</li>
		<li>infringe any copyright, moral right, database right, trade mark right, design right, right in passing off, or other intellectual property right;</li>
		<li>infringe any right of confidence, right of privacy or right under data protection legislation;</li>
		<li>constitute negligent advice or contain any negligent statement;</li>
		<li>constitute an incitement to commit a crime, instructions for the commission of a crime or the promotion of criminal activity;</li>
		<li>be in contempt of any court, or in breach of any court order;</li>
		<li>be in breach of racial or religious hatred or discrimination legislation;</li>
		<li>be in breach of official secrets legislation;</li>
		<li>be in breach of any contractual obligation owed to any person;</li>
		<li>depict violence in an explicit, graphic or gratuitous manner;</li>
		<li>be pornographic, lewd, suggestive or sexually explicit;</li>
		<li>be untrue, false, inaccurate or misleading</li>
		<li>consist of or contain any instructions, advice or other information which may be acted upon and could, if acted upon, cause illness, injury or death, or any other loss or damage</li>
		<li>constitute spam;</li>
		<li>be offensive, deceptive, fraudulent, threatening, abusive, harassing, anti-social, menacing, hateful, discriminatory or inflammatory; or</li>
		<li>cause annoyance, inconvenience or needless anxiety to any person.</li>
	</ul>
</p>

<div class="content-header" id="limited-warranties">Limited Waranties</div>
<p class="limited-warranties">
<ul style="list-style-type: square">
<li>We do not warrant or represent:</li>
<ul style="list-style-type: circle">
	<li>the completeness or accuracy of the information published on our website;</li>
	<li>that the material on the website is up to date; or</li>
	<li>that the website or any service on the website will remain available.</li>
</ul>
<li>We reserve the right to discontinue or alter any or all of our website services, and to stop publishing our website, at any time in our sole sdiscretion without notice or explanation; and save to the extent expressly provided otherwise in these terms and conditions, you will not be entitled to any compensation or other payment upon the discontinuance or alteration of any website services, or if we stop publishing the website.
</li>
<li>To the maximum extent permitted by applicable law and subject to <a href="TnC.php#Limitations-and-exlusions-of-liability">Limitations and exlusions of liability Section</a>, we exclude all representations and warranties relating to the subject matter of these terms and conditions, our website and the use of our website.</li>
</ul>
</p>

<div class="content-header" id="limitations-and-exlusions-of-liability">Limitations and exlusions of liability</div>
<p class="Limitations-and-exlusions-of-liability">
Nothing in these terms and conditions will:
<ul style="list-style-type: square">
	<li>limit or exclude any liability for death or personal injury resulting from negligence;</li>
	<li>limit or exclude any liability for fraud or fraudulent misrepresentation;</li>
	<li>limit any liabilities in any way that is not permitted under applicable law; or</li>
	<li>exclude any liabilities that may not be excluded under applicable law.</li>
</ul>

The limitations and exclusions of liability set out in this Section and elsewhere in these terms and conditions: 
<ul style="list-style-type: square">
<li>are subject to the <a href="TnC.php#Limitations-and-exlusions-of-liability">Section Above</a>; and</li>	
<li>govern all liabilities arising under these terms and conditions or relating to the subject matter of these terms and conditions, including liabilities arising in contract, in tort (including negligence) and for breach of statutory duty, except to the extent expressly provided otherwise in these terms and conditions.</li>
</ul>

To the extent that our website and the information and services on our website are provided free of charge, we will not be liable for any loss or damage of any nature.<br/>
We will not be liable to you in respect of any losses arising out of any event or events beyond our reasonable control.<br/>
We will not be liable to you in respect of any business losses, including (without limitation) loss of or damage to profits, income, revenue, use, production, anticipated savings, business, contracts, commercial opportunities or goodwill.<br/>
We will not be liable to you in respect of any loss or corruption of any data, database or software.<br/>
We will not be liable to you in respect of any special, indirect or consequential loss or damage.<br/>
You accept that we have an interest in limiting the personal liability of our officers and employees and, having regard to that interest, you acknowledge that we are a limited liability entity; you agree that you will not bring any claim personally against our officers or employees in respect of any losses you suffer in connection with the website or these terms and conditions (this will not, of course, limit or exclude the liability of the limited liability entity itself for the acts and omissions of our officers and employees).
</p>

<div class="content-header" id="breaches">Breaches of these terms and conditions</div>
<p class="breaches">
	Without prejudice to our other rights under these terms and conditions, if you breach these terms and conditions in any way, or if we reasonably suspect that you have breached these terms and conditions in any way, we may: 
	<ul style="list-style-type: square">
		<li>send you one or more formal warnings;</li>
		<li>temporarily suspend your access to our website;</li>
		<li>permanently prohibit you from accessing our website;</li>
		<li>block computers using your IP address from accessing our website;</li>
		<li>contact any or all of your internet service providers and request that they block your access to our website;</li>
		<li>commence legal action against you, whether for breach of contract or otherwise; and/or</li>
		<li>suspend or delete your account on our website.</li>
	</ul>
Where we suspend or prohibit or block your access to our website or a part of our website, you must not take any action to circumvent such suspension or prohibition or blocking (including without limitation creating and/or using a different account).
</p>

<div class="content-header" id="variation">Variation</div>
<p class="variation">
We may revise these terms and conditions from time to time.<br/><br/>
The revised terms and conditions shall apply to the use of our website from the date of publication of the revised terms and conditions on the website, and you hereby waive any right you may otherwise have to be notified of, or to consent to, revisions of these terms and conditions. The revised terms and conditions will apply to the use of our website from the date that we give you such notice; if you do not agree to the revised terms and conditions, you must stop using our website.<br/>
	If you have given your express agreement to these terms and conditions, we will ask for your express agreement to any revision of these terms and conditions; and if you do not give your express agreement to the revised terms and conditions within such period as we may specify, we will disable or delete your account on the website, and you must stop using the website.	
</p>

<div class="content-header" id="assignment">Assignment</div>
<p class="assignment">

You hereby agree that we may assign, transfer, sub-contract or otherwise deal with our rights and/or obligations under these terms and conditions.<br/> 
	You may not without our prior written consent assign, transfer, sub-contract or otherwise deal with any of your rights and/or obligations under these terms and conditions. 	
</p>

<div class="content-header" id="severability">Severability</div>
<p class="severability">
	If a provision of these terms and conditions is determined by any court or other competent authority to be unlawful and/or unenforceable, the other provisions will continue in effect.<br/>
	If any unlawful and/or unenforceable provision of these terms and conditions would be lawful or enforceable if part of it were deleted, that part will be deemed to be deleted, and the rest of the provision will continue in effect. 	
</p>

<div class="content-header" id="third-party-rights">Third party rights</div>
<p class="third-party-rights">
	A contract under these terms and conditions is for our benefit and your benefit, and is not intended to benefit or be enforceable by any third party.<br/>
	The exercise of the parties' rights under a contract under these terms and conditions is not subject to the consent of any third party.	
</p>

<div class="content-header" id="summary">Entire agreement</div>
	<p class="summary">
	Subject to <a href="TnC.php#limitations-and-exlusions-of-liability">limitations and Exlusions of Liability</a> section, these terms and conditions, together with our privacy and cookies policy, shall constitute the entire agreement between you and us in relation to your use of our website and shall supersede all previous agreements between you and us in relation to your use of our website.
	</p>

<div class="content-header" id="las-and-jurisdiction">Law and Jurisdiction</div>
<p class="law-and-jurisdiction">
These terms and conditions shall be governed by and construed in accordance with the Kenyan law.
Any disputes relating to these terms and conditions shall be subject to the exclusive jurisdiction of the courts of Kenya.
</p>
		
	</div>
</div>


<?php
include_once "inc/footer.php";
?>