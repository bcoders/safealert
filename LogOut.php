<?php
/**
 * Created by PhpStorm.
 * User: Coder
 * Date: 2/26/2018
 * Time: 12:51 AM
 */
require_once "globalIncs/sessions.php";
require_once 'sessions/functions.php';

session_destroy();
if (isset($_SESSION['loggedIn']))
{
    $_SESSION['loggedIn'] = False;
}

$functions->redirect("index.php");