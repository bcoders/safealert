<?php
/**
 * Created by PhpStorm.
 * User: Coder
 * Date: 2/26/2018
 * Time: 3:38 AM
 */
$pageName = "Privacy Policy";
require_once "inc/header.php";
?>
<style type="text/css">
a{
	text-decoration: underline;
}
.footer a{
	text-decoration: none;
}
.footer-bottom a{
	text-decoration: none;
}
p{
	font-family: serif;
	line-height: 1.5;
}
.row .privacy-policy{
	margin-left: 260px;
}
	.policy-header{
		font-family: sans-serif;
		font-weight: bold;
		margin-left: 10px;
	}
	.content-header{
		font-weight: bold;
		font-size: 150%;
		margin: 30px 0 25px 0;
	}
	.sub-header{
		font-weight: bold;
	}
</style>
<div class='row'>
	<div class="privacy-policy">
		<div class="policy-header" style="font-size:150%;text-align:center;">Privacy Policy</div>
		<div class="col-md-12">
			<div class="content-header" id="introduction">Introduction</div>
			<p id="section1">
				We are committed to safeguarding the privacy of <?php echo ucwords(_PROJECT);?> visitors and service users.
				This policy applies where we are acting as a data controller with respect to the personal data of <?php echo ucwords(_PROJECT);?> visitors and service users; in other words, where we determine the purposes and means of the processing of that personal data.
				We use cookies on our website. Insofar as those cookies are not strictly necessary for the provision of <?php echo ucwords(_PROJECT);?>, we will ask you to consent to our use of cookies when you first visit our website.
				Our website incorporates privacy controls which affect how we will process your personal data. By using the privacy controls, you can specify whether you would like to receive direct marketing communications and limit the publication of your information. .
				In this policy, "we", "us" and "our" refer to <?php echo ucwords(_PROJECT);?>. For more information about us, see our <a href="AboutUs.php">About</a> page.
			</p>
			<div class="content-header" id="data-usage">How we use your personal data</div>
<p class="data-usage">
In this Section we have set out:
<ul style="list-style-type: lower-alpha">
	<li>The general categories of personal data that we may process;</li>
	<li>In the case of personal data that we did not obtain directly from you, the source and specific categories of that data;</li>
	<li>The purposes for which we may process personal data; and </li>
	<li>The legal bases of the processing</li>
</ul>
Here are ways we may use personal data:
<ul style="list-style-type: disc;">
	<li>
		We may process data about your use of our website and services ("usage data"). The usage data may include your IP address, geographical location, browser type and version, operating system, referral source, length of visit, page views and website navigation paths, as well as information about the timing, frequency and pattern of your service use. The source of the usage data is our analytics tracking system. This usage data may be processed for the purposes of analysing the use of the website and services. The legal basis for this processing is consent OR our legitimate interests, namely monitoring and improving our website and services.
	</li>
	<li>
		We may process your account data ("account data"), which may include your name and email address. The account data may be processed for the purposes of operating our website, providing our services, ensuring the security of our website and services, maintaining back-ups of our databases and communicating with you. The legal basis for this processing is consent OR our legitimate interests, namely the proper administration of our website and business OR the performance of a contract between you and us and/or taking steps, at your request, to enter into such a contract OR specify basis.
	</li>
	<li>
		We may process your information included in your personal profile on our website ("profile data"). The profile data may include your name, address, telephone number, email address. The profile data may be processed for the purposes of enabling and monitoring your use of our website and services. The legal basis for this processing is consent OR our legitimate interests, namely the proper administration of our website and business OR the performance of a contract between you and us and/or taking steps, at your request, to enter into such a contract OR other uses we may specify here.
	</li>
	<li>
		We may process your personal data that are provided in the course of the use of our services ("service data"). The service data may include specify data. The source of the service data is you. The service data may be processed for the purposes of operating our website, providing our services, ensuring the security of our website and services, maintaining back-ups of our databases and communicating with you. The legal basis for this processing is consent OR our legitimate interests, namely the proper administration of our website and business OR the performance of a contract between you and us and/or taking steps, at your request, to enter into such a contract OR otherwise specified here.
	</li>
	<li>
		We may process information that you post for publication on our website or through our services ("publication data"). The publication data may be processed for the purposes of enabling such publication and administering our website and services. The legal basis for this processing is consent OR our legitimate interests, namely the proper administration of our website and business OR the performance of a contract between you and us and/or taking steps, at your request, to enter into such a contract OR otherwise herein specified.
	</li>
	<li>
		We may process information contained in any enquiry you submit to us regarding goods and/or services ("enquiry data"). The enquiry data may be processed for the purposes of offering, marketing or/and selling relevant goods and/or services to you. The legal basis for this processing is consent OR otherwise herein specified.
	</li>
	<li>
		We may process information that you provide to us for the purpose of subscribing to our email notifications and/or newsletters ("notification data"). The notification data may be processed for the purposes of sending you the relevant notifications and/or newsletters. The legal basis for this processing is consent OR the performance of a contract between you and us and/or taking steps, at your request, to enter into such a contract OR herein specified.
	</li>
	<li>
		We may process information contained in or relating to any communication that you send to us ("correspondence data"). The correspondence data may include the communication content and metadata associated with the communication. Our website will generate the metadata associated with communications made using the website contact forms. The correspondence data may be processed for the purposes of communicating with you and record-keeping. The legal basis for this processing is our legitimate interests, namely the proper administration of our website and business and communications with users OR otherwise herein specified.
	</li>
	<li>
		We may process any of your personal data identified in this policy where necessary for the establishment, exercise or defence of legal claims, whether in court proceedings or in an administrative or out-of-court procedure. The legal basis for this processing is our legitimate interests, namely the protection and assertion of our legal rights, your legal rights and the legal rights of others.
	</li>
	<li>
		We may process any of your personal data identified in this policy where necessary for the purposes of obtaining or maintaining insurance coverage, managing risks, or obtaining professional advice. The legal basis for this processing is our legitimate interests, namely the proper protection of our business against risks.
	</li>
	<li>
		In addition to the specific purposes for which we may process your personal data set out in this Section, we may also process any of your personal data where such processing is necessary for compliance with a legal obligation to which we are subject, or in order to protect your vital interests or the vital interests of another natural person.
	</li>
</ul>
</p>
<div class="content-header" id="disclosing-your-info">Providing your personal data to others</div>
<p>
<ul style="list-style-type: disc">
	<li>
		We may disclose your personal data to any member of our group of companies (this means our subsidiaries, our ultimate holding company and/or all its subsidiaries) insofar as reasonably necessary for the purposes, and on the legal bases, set out in this policy.
	</li>
	<li>
		We may disclose your personal data to our insurers and/or professional advisers insofar as reasonably necessary for the purposes of obtaining or maintaining insurance coverage, managing risks, obtaining professional advice, or the establishment, exercise or defence of legal claims, whether in court proceedings or in an administrative or out-of-court procedure.
	</li>
	<li>
		We may disclose your enquiry data to one or more of those selected third party suppliers of goods and services identified on our website] for the purpose of enabling them to contact you so that they can offer, market and sell to you relevant goods and/or services, with your prior consent. We only do so after you authorise us to do so. Each such third party will act as a data controller in relation to the enquiry data that we supply to it; and upon contacting you, each such third party will supply to you a copy of its own privacy policy, which will govern that third party's use of your personal data.
	</li>
	<li>
		In addition to the specific disclosures of personal data set out in this Section, we may disclose your personal data where such disclosure is necessary for compliance with a legal obligation to which we are subject, or in order to protect your vital interests or the vital interests of another natural person. We may also disclose your personal data where such disclosure is necessary for the establishment, exercise or defence of legal claims, whether in court proceedings or in an administrative or out-of-court procedure.
	</li>
</ul>	
</p>

<div class="content-header" id="data-retention">Retaining and deleting personal data</div>
<p class="data-retention">
<b>This Section sets out our data retention policies and procedure, which are designed to help ensure that we comply with our legal obligations in relation to the retention and deletion of personal data.</b>
	<ul style="list-style-type: disc">
		<li>
			You acknowledge that personal data that you submit for publication through our website or services may be available, via the internet, around the world. We cannot prevent the use (or misuse) of such personal data by others.
		</li>
		<li>
			Personal data that we process for any purpose or purposes shall not be kept for longer than is necessary for that purpose or those purposes.
		</li>
	</ul>
	<span class="sub-header">We will retain your personal data as follows:</span>
<ul style="list-style-type: disc">
	<li>
		Personal data recorded when creating user accounts will be retained for as long as your account exists with us.
	</li>
	<li>
		Notwithstanding the other provisions of this Section, we may retain your personal data where such retention is necessary for compliance with a legal obligation to which we are subject, or in order to protect your vital interests or the vital interests of another natural person.
	</li>
</ul>
</p>

<div class="content-header">Amendments</div>
<p class="amendments">
	We may update this policy from time to time by publishing a new version on our website.<br/>
	You should check this page occasionally to ensure you are happy with any changes to this policy.<br/>
	We will notify you of changes to this policy by email or through the notification system on our website.
</p>

<div class="content-header">Your rights</div>
<p class="your-rights">
In this Section, we have summarised the rights that you have under data protection law. Some of the rights are complex, and not all of the details have been included in our summaries. Accordingly, you should read the relevant laws and guidance from the regulatory authorities for a full explanation of these rights.<br/>
	<ul style="list-style-type: disc">
		<li>
			8.2	Your principal rights under data protection law are:
		</li>
		<ul style="list-style-type: round;">
			<li>the right to access;</li>
			<li>the right to rectification;</li>
			<li>the right to erasure;</li>
			<li>the right to restrict processing;</li>
			<li>the right to object to processing;</li>
			<li>the right to data portability;</li>
			<li>the right to complain to a supervisory authority; and</li>
			<li>the right to withdraw consent.</li>
		</ul>
		<li>You have the right to confirmation as to whether or not we process your personal data and, where we do, access to the personal data, together with certain additional information. That additional information includes details of the purposes of the processing, the categories of personal data concerned and the recipients of the personal data. Providing the rights and freedoms of others are not affected, we will supply to you a copy of your personal data. The first copy will be provided free of charge, but additional copies may be subject to a reasonable fee. You can access your personal data by visiting <a href='MyAccount.php'>My Account</a> when logged into our website.</li>
		<li>
			You have the right to have any inaccurate personal data about you rectified and, taking into account the purposes of the processing, to have any incomplete personal data about you completed.
		</li>
		<li>
			In some circumstances you have the right to the erasure of your personal data without undue delay. Those circumstances include: 
			<ul style="list-style-type: square">
				<li>personal data are no longer necessary in relation to the purposes for which they were collected or otherwise processed;</li>
				<li>you withdraw consent to consent-based processing; you object to the processing under certain rules of applicable data protection law;</li>
				<li>the processing is for direct marketing purposes;</li>
				<li>and the personal data have been unlawfully processed.</li>
			</ul>     However, there are exclusions of the right to erasure. 
			The general exclusions include where processing is necessary: 
			<ul style="list-style-type: square">
			<li>for exercising the right of freedom of expression and information;</li>
			<li>for compliance with a legal obligation; or</li>
			<li>for the establishment, exercise or defence of legal claims.</li>	
			</ul>
		</li>
		<li>
			In some circumstances you have the right to restrict the processing of your personal data. Those circumstances are: 
			<ul style="list-style-type: square">
				<li>contest the accuracy of the personal data;</li>
				<li>processing is unlawful but you oppose erasure;</li>
				<li>we no longer need the personal data for the purposes of our processing, but you require personal data for the establishment, exercise or defence of legal claims;</li>
				<li>you have objected to processing, pending the verification of that objection.</li>
			</ul> 
			Where processing has been restricted on this basis, we may continue to store your personal data. However, we will only otherwise process it: 
			<ul style="list-style-type: square">
				<li>with your consent;</li>
				<li>for the establishment, exercise or defence of legal claims;</li>
				<li>for the protection of the rights of another natural or legal person;</li>
				<li>or for reasons of important public interest.</li>
			</ul>    
		</li>
		<li>
			You have the right to object to our processing of your personal data on grounds relating to your particular situation, but only to the extent that the legal basis for the processing is that the processing is necessary for: 
			<ul style="list-style-type: square">
				<li>the performance of a task carried out in the public interest or in the exercise of any official authority vested in us;</li>
				<li>or the purposes of the legitimate interests pursued by us or by a third party.</li>
			</ul>
			  If you make such an objection, we will cease to process the personal information unless we can demonstrate compelling legitimate grounds for the processing which override your interests, rights and freedoms, or the processing is for the establishment, exercise or defence of legal claims.
		</li>
		<li>
			You have the right to object to our processing of your personal data for direct marketing purposes (including profiling for direct marketing purposes). If you make such an objection, we will cease to process your personal data for this purpose.
		</li>
		<li>
			You have the right to object to our processing of your personal data for scientific or historical research purposes or statistical purposes on grounds relating to your particular situation, unless the processing is necessary for the performance of a task carried out for reasons of public interest.
		</li>
		<li>
			To the extent that the legal basis for our processing of your personal data is:
			<ul style="list-style-type: square">
				<li>consent; or</li>
				<li>that the processing is necessary for the performance of a contract to which you are party or in order to take steps at your request prior to entering into a contract,
	and such processing is carried out by automated means, you have the right to receive your personal data from us in a structured, commonly used and machine-readable format. However, this right does not apply where it would adversely affect the rights and freedoms of others.</li>
			</ul>
		</li>
		<li>
			If you consider that our processing of your personal information infringes data protection laws, you have a legal right to lodge a complaint with a supervisory authority responsible for data protection. You may do so in the EU member state of your habitual residence, your place of work or the place of the alleged infringement.
		</li>
		<li>
			To the extent that the legal basis for our processing of your personal information is consent, you have the right to withdraw that consent at any time. Withdrawal will not affect the lawfulness of processing before the withdrawal.
		</li>
		<li>
			You may exercise any of your rights in relation to your personal data by written notice to us, in addition to the other methods specified in this Section.
		</li>
	</ul>
</p>

<div class="content-header" id="about-cookies">About Cookies</div>
<p class="about-cookies">
See: <a href="CookiePolicy.php?source=pp"><?php echo $_SERVER['HTTP_HOST'] . '/' . _PROJECT . 'CookiePolicy.php' ?></a> for info on cookies and how we handle cookies on our site.
</p>

		</div>
		</div>
</div>
<?php
require_once "inc/footer.php";
?>
