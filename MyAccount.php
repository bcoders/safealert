<?php
/**
 * Created by PhpStorm.
 * User: Coder
 * Date: 10/10/2018
 * Time: 3:57 PM
 */
$pageName = 'My Account';

include_once "inc/header.php";
include_once "sessions/Db.class.php";
include_once "sessions/Bcrypt.php";
$functions->RequireLoggedInAccess();
?>
<div class="row" style="margin: 20px 0 0 0;width: 70%;">
    <div class="col-md-4">
        <div class="panel panel-default">
            <div class="panel-heading">
                Email
            </div>
            <div class="panel-body">
                <input type="text" disabled="disabled" value="<?php echo $_SESSION['userEmail'];?>" class="form-control"/>
            </div>
        </div>
    </div>

    <div class="col-md-8">
        <div class="panel panel-info" style="background: #808d8b">
            <?php
            if (isset($_POST['changepass']))
            {
                $userPasswordHashFrommDb = $db->single("SELECT password FROM users WHERE email=:email", array("email"=>$_SESSION['userEmail']));
                $userSuppliedPass        = $_POST['oldpassword'];
                if (empty($_POST['oldpassword'])){$oError[]="Please provide your old password";}
                elseif (empty($_POST['newpassword'])){$oError[]="You must provide a new password";}
                elseif (empty($_POST['rpnewpassword'])){$oError[]="Confirm your new password";}
                elseif ($_POST['newpassword'] !== $_POST['rpnewpassword']){$oError[]="The passwords do not match";}
                elseif (!Bcrypt::checkPassword($userSuppliedPass, $userPasswordHashFrommDb)){$oError[] = "The password does not match our records";}
                else {
                    // save the new password now
                    $newPasswordHash = Bcrypt::hashPassword($_POST['newpassword']);
                    if ($db->query("UPDATE users SET password=:newpassword WHERE email=:userEmail", array('newpassword'=>$newPasswordHash, 'userEmail'=>$_SESSION['userEmail']))){
                        $successMsg = "You have succesfully changed your password";
                    }
                    else{
                        $oError[] = "An error occurred";
                    }
                }
            }
            ?>
            <div class="panel-heading">
                <b>Change Password</b>
            </div>
            <div class="panel-body">
                <div class="panel" style="background-color: inherit !important; width: 100%;">
                    <?php
                    if (isset($oError)):
                        foreach ($oError as $item):
                            ?>
                             <span class="alert alert-danger col-md-12 col-sm-12 col-xs-12"><i class="fa fa-warning"></i>&ensp; <?php echo $item; ?></span>
                        <?php
                        endforeach;
                     // end oError isset
                    elseif (isset($successMsg)):
                    ?>
                    <span class="alert alert-success col-md-12 col-sm-12 col-xs-12"> <?php echo $successMsg; ?></span>
                    <?php
                    endif;
                    ?>
                </div>
                <form method="post" autocomplete="off">
                    <div class="form-group">
                        <input type="password" name="oldpassword" class="form-control" placeholder="Old Password..." id="pfield" required="required"/>
                    </div>
                    <div class="form-group">
                        <input type="password" name="newpassword" class="form-control" placeholder="New Password..." id="pfield" required="required"/>
                    </div>
                    <div class="form-group">
                        <input type="password" name="rpnewpassword" class="form-control" placeholder="New Password Again..." id="pfield" required="required"/>
                    </div>
                    <button class="btn btn-primary" type="submit" name="changepass" id="changepass">Change Pass</button>
                </form>
            </div>
        </div>
    </div>
</div>

<?php
include_once "inc/footer.php";
?>