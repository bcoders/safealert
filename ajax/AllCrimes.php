<?php

/*
 * Show all errors (not required of course)
 */
ini_set('display_errors','On');
error_reporting(-1);


/*
 * Include the pagination.php class file
 */
require_once('' . '../sessions/Pagination.php');
require_once('' . '../sessions/functions.php');


/*
 * Connect to the database (Replacing the XXXXXX's with the correct details)
 */
try
{
    $dbh = new PDO('mysql:host=localhost;dbname=krime', 'blancos', 'blancoskhim');
    $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
}
catch(PDOException $e)
{
    print "Error!: " . $e->getMessage() . "<br/>";
}


/*
 * Get and/or set the page number we are on
 */
if(isset($_GET['page']))
{
    $page = $_GET['page'];
}
else
{
    $page = 1;
}


/*
 * Set a few of the basic options for the class, replacing the URL with your own of course
 */
$options = array(
    'results_per_page' => 6,
    'url' => 'http://' . $_SERVER['HTTP_HOST'] . '/ToDo/index.php?page=*VAR*',
    'db_handle' => $dbh
);


/*
 * Create the pagination object
 */
$paginate = new pagination($page, 'SELECT * FROM crimes ORDER BY ID DESC ', $options);

/*
 * If we get a success, carry on
 */
if($paginate->success == true)
{
    /*
     * Fetch our results
     */
    $result = $paginate->resultset->fetchAll();

    /*
     * Work with our data rows
     */
    foreach($result as $row):
        // get the userID and get the username of the user who posted the crime
        $userName = $functions->GetUserNameByID($row['postedByID']);
        $postingdate  = $functions->GetCrimePostingDate($row['ID']);
    ?>
        <div class="panel panel-info" id="crime-list">
            <!--<div class="panel-heading text-right top-header">
                <i class="fa fa-bullhorn"></i>&nbsp;Posted <?php echo /*'<b>' . ucwords($userName) . '</b>*/' on <b>' . $postingdate . '</b>';?>
            </div>-->
            <div class="panel-heading bottom-header">
                <?php echo $row['slug'];?>
            </div>
            <div class="panel-body">
                <?php
                $fullDescription = $row['description'];
                if (strlen($fullDescription) > 699):
                    $pos = strpos($fullDescription, ' ', 699);
                    $trimmedDescr = substr($fullDescription, 0, $pos);
                    $crimeID = base64_encode($row['ID']);
                    $slug= base64_encode($row['slug']);
                    $timeOccured = base64_encode($row['timeOccurred']);

                    echo <<<EOT
 <p> . $trimmedDescr . ......</p>
                <a href="Crime.php?CiD=$crimeID&description=$slug&occt=$timeOccured" class="btn btn-info"> Explore Crime</a>
EOT;

                else:
                    $crimeID = base64_encode($row['ID']);
                    $slug= base64_encode($row['slug']);
                    $timeOccured = base64_encode($row['timeOccurred']);

                    echo <<<EOT
 $fullDescription . <br/> 
                <a href="Crime.php?CiD=$crimeID&description=$slug&occt=$timeOccured" class="btn btn-info"> Explore Crime</a>
EOT;


                endif;
                    ?>
            </div>
        </div>

<?php
endforeach;
    /*
        * Echo out the UL with the page links
        */
    //echo '<p>'.$paginate->links_html.'</p>';


}
?>

