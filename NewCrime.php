<?php
/**
 * Created by PhpStorm.
 * User: Coder
 * Date: 2/26/2018
 * Time: 3:38 AM
 */
$pageName = "Add A New Entry";
require_once "inc/header.php";
//only logged in users can record a crime
$functions->RequireLoggedInAccess();
require_once "sessions/Db.class.php";

/**
 * handle the crime saving to db/validation/sanitization
 *
 * first the form has to be 'POSTed'
 */
if (isset($_POST['record-crime']))
{
    // instantiate values
    $currentTimeDate= date('Y M D H:i:s');
    $slug           = $functions->sanitise($_POST['crime-slug']);
    //if (isset($_POST['crime-description']))
    $description    = $functions->sanitise($_POST['crime-description']);
    $dateCommitted  = $_POST['date-committed'];
    $timeCommitted  = $_POST['time-committed'];
    $location       = $functions->sanitise($_POST['place-committed']);
    
    $response  = $_POST["g-recaptcha-response"];
    
    $url = 'https://www.google.com/recaptcha/api/siteverify';
	$data = array(
		'secret' => '6Ldk0n8UAAAAAJOley79M8ooRTS2hMEl5VfcU5sL',
		'response' => $_POST["g-recaptcha-response"]
	);
	$options = array(
		'http' => array (
			'method' => 'POST',
			'content' => http_build_query($data)
		)
	);
	$context  = stream_context_create($options);
	$verify = file_get_contents($url, false, $context);
	$captcha_success=json_decode($verify);


    // validate the data
    if (empty($slug)){
        $ioError[] = 'You have to categorise the crime please';
    }
    elseif(empty($_POST['crime-description'])){
        $ioError[] = 'The description cannot be empty';
    }
    elseif(empty($dateCommitted)){
        $ioError[] = 'The date the crime occurred is empty';
    }
    elseif(empty($timeCommitted)){
        $ioError[] = 'The approximate time must be provided';
    }
    elseif(empty($location)){
        $ioError[] = 'The location must be provided';
    }
    elseif($captcha_success->success==false){
		$oError[] = "We couldn't verify you are human";
	}
    else{
        /**
         * just save the data to the database
         * we aren't really handling any more stuff
         */
        try{
        $db->query("INSERT INTO `crimes` (`postedByID`,`slug`,`description`,`datePosted`,`timeOccurred`, `dateOccurred`, `location`) VALUES (:userId,:slug,:description,:today,:timeOccurred,:dateOccurred,:location)", array(
            'userId'        => $functions->getUserIDFromEmail($_SESSION['userEmail']),
            'slug'          => $slug,
            'description'   => $description,
            'today'         => $currentTimeDate,
            'timeOccurred'  => $timeCommitted,
            'dateOccurred'  => $dateCommitted,
            'location'      => $location,
        ));
        $functions->redirect('index.php');
        }
        catch (PDOException $e)
        {
            echo $e->getMessage();
        }

    }
}
?>

    <div class="col-md-1"></div>
    <div class="col-md-9">
        <?php
        if (isset($oError)):
            foreach ($oError as $error):?>
                <div class="alert alert-danger col-md-12 col-sm-12 col-xs-12">&ensp;
                    <i class="fa fa-warning"></i>&nbsp;<?php echo $error . "!"; ?>
                </div>
            <?php
            endforeach;
        endif;
        ?>
        <form class="crime-record-form" method="post" name="recordCrime">
            <div class="form-header" style="padding:7px;font-weight: bolder;font-size: 130%;">
                Fill Out The Form To Record A Crime
            </div>
            <fieldset name="fieldset">
                <div class="form-group">
                    <label for="">Crime Intro Here</label>
                        <input type="text" name="crime-slug" id="crime-slug" class="form-control"  placeholder="Burglary / Theft / Kidnapping / Break-In / Hit-and-Run" required="required"/>
                </div>
                <div class="form-group">
                    <label for="">Describe the crime details here</label><br/>
                    <textarea name="crime-description" id="crime-description" required="required"></textarea>
                </div>

                <div class="ro crime-record-row"">
                    <div class="col-md-2 crime-date">
                        <div class="form-group">
                            <label for="">What date-time did the crime occur?</label><br/>
                            <input type="date" id="datepicker" name="date-committed" class="" required="required" /> at
                            <input type="time" id="time-committed" name="time-committed" class="" required="required" />
                        </div>
                    </div>
                    <div class="col-md-5 crime-location">
                        <div class="form-group">
                            <label for="">Location of the crime</label><br/>
                            <input type="text" id="place-committed" name="place-committed" class="form-control" required="required" placeholder="Kasarani Opp TRM next to Armanet Stalls" style="font-size: 90%;color: #0f0f0f"/>
                        </div>
                    </div>
                </div>

                <div class="col-md-2">
                <div class="form-group">
                    <label for="">Verify you are human</label>
                    	<div class="g-recaptcha" data-sitekey="6Ldk0n8UAAAAANiwctmahzfpVb5u8cmZ06qMPcvP"></div>
                    </div>
                    <button class="btn btn-primary" id="record-crime" name="record-crime" value="Record Crime">Record Crime</button>
                </div>
            </fieldset>
        </form>
    </div>
    <div class="col-md-2"></div>

<?php
require_once "inc/footer.php";
?>
