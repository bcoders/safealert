<?php
/**
 * Created by PhpStorm.
 * User: Coder
 * Date: 2/21/2018
 * Time: 6:08 AM
 */
$pageName = "Home";
//call the header first
require_once 'inc/header.php';

/**
 * include the LeftSideBar.inc file that houses our left sidebar contents
 */

?>

<div class="row main-contents">
    <div class="col-md-9 content-mid">
        <?php
        /**
         * if user is logged in (including admin)
         * allow them to record a crime
         */
        if (isset($_SESSION))
            if(isset($_SESSION['loggedInToUs'])):?>
            <div class="text-right" style="margin:0 0 5px 0;">
                <a href="NewCrime.php" class="btn btn-primary">Record A New Crime</a>
            </div>
        <?php
            endif; ?>
        <?php
        /**
         * we list the crimes in our database here
         * I have to implement a kind of pagination to limit the data that shows, or
         * use Ajax for infinite scrolling
         * (will decide later thoo)
         */

        /**
         * include the database class here
         * I donno why but I tend to believe it uses less resources this way
         */
        require_once "sessions/Db.class.php";


        ?>
        <div class="crime-details" id="crime-details"><?php require_once 'inc/AllCrimes.php'; ?></div>
    </div>

    <div class="col-md-3 content-right" id="ad-sidebar">
        <?php include_once "inc/advert.inc.php";?>
    </div>
</div>

    <script type = "text/javascript" language = "javascript">

            var auto_refresh = setInterval(
                function () {
                    $('#crime-list')
                        .load('ajax/AllCrimes.php')
                }, 15000); // refreshing every 5000 milliseconds/5 seconds

            /**$('#crime-details').load('ajax/AllCrimes.php').fadeIn('slow').setInterval(2000);**/

    </script>

<?php
// footer comes last
require_once 'inc/footer.php';
