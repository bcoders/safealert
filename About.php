<?php
/**
 * Created by PhpStorm.
 * User: Coder
 * Date: 2/23/2018
 * Time: 5:49 AM
 */
$pageName = "About KrimE";
require_once "inc/header.php";

// our SafeAlert.class
require_once 'sessions/SafeAlert.class.php';
$weAreSafer -> getPageContent($pageName='about-us');

// our footer
require_once "inc/footer.php";
