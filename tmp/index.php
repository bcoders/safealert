<html>
<head>
    <title>The jQuery Ajax Example</title>
    <script type = "text/javascript"
            src = "../assets/js/jquery.js">
    </script>

    <script type = "text/javascript" language = "javascript">
        $(document).ready(function() {
            $("#name").keyup(function(event){
                    var name = $("#name").val();
                    $("#stage").load('ajax.php', {"name":name} );
                });
        });
    </script>
</head>

<body>
<p>Enter your name and click on the button:</p>
<input type = "text" id = "name" size = "40"/><br />

<div id = "stage" style = "background-color:#cc0;">
    STAGE
</div>

<input type = "button" id = "driver" value = "Show Result" />
</body>
</html>