<?php
/**
 * Created by PhpStorm.
 * User: Coder
 * Date: 2/23/2018
 * Time: 5:28 AM
 */
$pageName = "Create Account";
require_once "inc/header.php";
/**
 * only logged out users can access this page
 */
$functions->RequireLoggedOutAccess();
// validating the user input, saving to DB
if (isset($_POST['login']))
{
    // this file needs the database classes to insert read and do db operations
    require_once 'sessions/Db.class.php';
    // we also need the bcrypt class to hash our password
    require_once "sessions/Bcrypt.php";
    // this file handles our sanitization issues
    require_once 'sessions/functions.php';

    $firstname = $functions->sanitise($_POST['firstname']);
    $lastname  = $functions->sanitise($_POST['lastname']);
    $email     = $functions->sanitise($_POST['email']);
    $username  = $functions->sanitise($_POST['username']);
    $password  = $_POST['password'];

    $response  = $_POST["g-recaptcha-response"];

    $url = 'https://www.google.com/recaptcha/api/siteverify';
    $data = array(
        'secret' => '6Ldk0n8UAAAAAJOley79M8ooRTS2hMEl5VfcU5sL',
        'response' => $_POST["g-recaptcha-response"]
    );
    $options = array(
        'http' => array (
            'method' => 'POST',
            'content' => http_build_query($data)
        )
    );
    $context  = stream_context_create($options);
    $verify = file_get_contents($url, false, $context);
    $captcha_success=json_decode($verify);

    if (empty($_POST['firstname']))
    {
        $oError[] = "You must provide your First Name";
    }
    elseif (empty($_POST['lastname']))
    {
        $oError[] = "You must provide your Last Name";
    }
    elseif (empty($_POST['email']))
    {
        $oError[] = "You must provide an Email Address";
    }
    elseif(!filter_var($email,FILTER_VALIDATE_EMAIL))
    {
        $oError[] = "Invalid email format";
    }

    elseif (empty($_POST['username']))
    {
        $oError[] = "You must provide a password";
    }
    elseif (empty($_POST['password']))
    {
        $oError[] = "You must provide a password";
    }
    elseif ($_POST['password']!= $_POST['rpassword'])
    {
        $oError[] = "The passwords do not match";
    }
    elseif($captcha_success->success==false){
        $oError[] = "We couldn't verify you are human";
    }
    // if all the above validate as true, move on to the next step
    else{
        // handle db
        // check if that email exists in our db

            if ($db->row("SELECT email FROM users WHERE email = :email",array("email"=>$email)))
            {
                $oError[] = "That email is already registered with us";
            }
        elseif ($db->row("SELECT username FROM users WHERE username = :username",array("username"=>$username)))
        {
            $oError[] = "That username exists";
        }
            else
            {
                // we can hash the password
                $passhash = Bcrypt::hashPassword($password,12);
                $savetodb = $db->query("INSERT INTO users(firstname,lastname,email,username,dateCreated,password) VALUES(:f,:l,:e,:u,:d,:p)",
                    array(
                         "f"=>$firstname,
                        "l"=>$lastname,
                        "e"=>$email,
                        "u"=>$username,
                        "d"=>date('Y M D H:i:s'),
                        "p"=>$passhash
                    ));
                if($savetodb > 0 ) {
                    $success = 'Successfully created an account with us';
                    $functions->redirect('SignIn.php');
                }
                else
                {
                    echo "Error while registering you";
                }
            }
    }
}
?>

        <div class="col-sm-3"></div>
        <div class="col-sm-6 signup-form">
            <div class="form-box">
                <div class="form-top">
                    <div class="form-top-left">
                        <span class="form-header">REGISTER</span>
                    </div>
                </div>
                <?php
                if (isset($oError)):
                    foreach ($oError as $error):?>
                        <span class="alert alert-danger col-md-12 col-sm-12 col-xs-12"><i class="fa fa-warning"></i>&ensp; <?php echo $error; ?></span>
                <?php
                endforeach;
                elseif (isset($success)):
                ?>
                    <span class="alert alert-danger col-md-12 col-sm-12 col-xs-12"><i class="fa fa-warning"></i>&ensp; <?php echo $success; ?></span>
                <?php
                endif;
                ?>
                <div class="form-bottom">
                    <form role="form" method="post" class="registration-form" autocomplete="off">
                        <div class="form-group">
                            <label class="sr-only" for="form-first-name">First name</label>
                            <input type="text" name="firstname" placeholder="First name..." class="form-first-name form-control" id="firstname" value="<?php if(isset($firstname)){echo $firstname;} ?>" pattern="[a-zA-Z]*" maxlength="15" required="required" />
                        </div>
                        <div class="form-group">
                            <label class="sr-only" for="form-last-name">Last name</label>
                            <input type="text" name="lastname" placeholder="Last name..." class="form-last-name form-control" id="lastname" value="<?php if(isset($lastname)){echo $lastname;} ?>" pattern="[a-zA-Z]*" maxlength="15" required="required"/>
                        </div>
                        <div class="form-group">
                            <label class="sr-only" for="form-email">Email</label>
                            <input type="email" name="email" placeholder="Email..." class="form-email form-control" id="email" value="<?php if(isset($email)){echo $email;} ?>"  required="required"/>
                        </div>
                        <div class="form-group">
                            <label class="sr-only" for="form-username">UserName</label>
                            <input type="text" name="username" placeholder="Username..." class="form-username form-control" id="username" required="required" value="<?php if(isset($username)){echo $username;} ?>" maxlength="13" />
                        </div>
                        <div class="form-group">
                            <label class="sr-only" for="form-password">Password</label>
                            <input type="password" name="password" placeholder="Password..." class="form-password form-control" id="password" required="required">
                        </div>
                        <div class="form-group">
                            <label class="sr-only" for="form-password">Repeat Password</label>
                            <input type="password" name="rpassword" placeholder="Repeat Password..." class="form-password form-control" id="rpassword" required="required">
                        </div>
                        <div class="form-group">
                            <label for="">Verify you are human</label>
                            <div class="g-recaptcha" data-sitekey="6Ldk0n8UAAAAANiwctmahzfpVb5u8cmZ06qMPcvP"></div>
                        </div>

                        <button type="submit" class="btn btn-primary" name="login">Sign me up!</button>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-sm-3"></div>

<?php
require_once "inc/footer.php";